<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->longText('recommend');
            $table->longText('content');
            $table->string('address');
            $table->string('direction');
            $table->string('document');
            $table->integer('area');  
            $table->integer('bedroom')->nullable();
            $table->integer('bathroom')->nullable();  
            $table->bigInteger('price');
            $table->integer('status');
            $table->integer('pin');
            $table->integer('view');
            $table->double('lat')->nullable();
            $table->double('long')->nullable();
            $table->integer('category_id');
            $table->integer('user_id');
            $table->integer('district_id');
            $table->date('start_date');
            $table->date('end_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
