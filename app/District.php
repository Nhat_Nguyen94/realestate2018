<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class District extends Model
{
 	public function city()
    {
        return $this->belongsTo(City::class,'city_id');
    }

    public	function article()
    {
    	return $this->hasMany(Article::class);
    }
}
