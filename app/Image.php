<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable = ['src', 'alt'];
    public function imageable()
    {
        return $this->morphTo();
    }
}
