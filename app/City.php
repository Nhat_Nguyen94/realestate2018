<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class City extends Model
{
	// protected $table='city';

    public function district()
    {
        return $this->hasMany('App\District');
    }
}
