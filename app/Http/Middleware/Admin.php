<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $check = null)
    {
        if (!Auth::check()) {
            return redirect()->route('authen.login');
        } else {
            if (Auth::user()->level == 1) {
                Auth::logout();
                return redirect()->route('authen.login');
            } else {
                return $next($request);
            }

        }
    }
}
