<?php

namespace App\Http\Controllers;

use Mail;
use App\User;
use App\Mail\OrderShipped;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
 
class EmailController extends Controller
{
      public function __construct()
    {
        $this->middleware('auth');
    }

      public function sendEmailReminder(Request $request)
    {
    	// dd($request->all());
        $id = 3; // điền 1 mã id bất kỳ của user trong bảng users 
        $user = User::find($id);
        // dd($user->name);

        // foreach ($user as $value) {
        //     dd($value->name);
        // }
         Mail::send('emails.sendemail', ['user' => $user], function ($m) use ($user) {
            // dd($m);
            $m->from('admin@gmail.com', 'Hello'.$user->name);

            $m->to($user->email, $user->name)->subject('Your Reminder!');
        });
    }
}
