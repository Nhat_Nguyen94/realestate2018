<?php

namespace App\Http\Controllers\Realestate;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\City;
use App\District;
use App\Article;
use App\User;
use Illuminate\Support\Facades\DB;
class ArticlesController extends Controller
{
    public function index(){
    	$article = Article::select()
        // ->whereDate('start_date', '<=', date('Y-m-d'))
        // ->whereDate('end_date', '>=', date('Y-m-d'))
        ->where('status',1)->orderBy('pin', 'DESC')->orderBy('created_at', 'DESC')->paginate(4);
    	 return view('realestate.index', compact('article'));
    }

     public function searchDistrict(Request $request){
    	$id = $request->id;
    	$district = new District();
         $dist = $district->select()->where('city_id',$id)->get();
         return json_encode($dist);
    }

    public function sortByCategory($id){
        $sortCate = Article::select()->where('category_id','=',$id)
        ->whereDate('start_date', '<=', date('Y-m-d'))
        ->whereDate('end_date', '>=', date('Y-m-d'))
        ->orderBy('pin', 'DESC')->orderBy('created_at', 'DESC')
        ->where('status',1)->paginate(3);
        // dd($sortCate);
       return view('realestate.sortCategory.list',compact('sortCate'));

    }

    public function detail($id){
        // dd($id);
        $article = Article::find($id);
        $category = Category::select()->where('status',1)->get();
        $city = City::all();
        return view('realestate.detailArticle.list',compact('category','city','article'));
    }

    public function searchArticle(Request $request){
        //dd($request->all());
       $query = Article::select('articles.*');

        if($request->txtName != null) {
            $query = $query->where('articles.title','like','%' . $request->txtName . '%');
        }
        if($request->select_city != null && $request->select_city != "all") {
            $query = $query
                        ->join('districts', 'articles.district_id','=','districts.id')
                        ->join('cities', 'districts.city_id','=','cities.id')
                        ->where('cities.id',$request->select_city);
        }
        if($request->select_district != null && $request->select_district != "all") {
            $query = $query->where('articles.district_id',$request->select_district);
        }
        if($request->category != null) {
            $query = $query->where('articles.category_id', $request->category);
        }
        if($request->min_price != null) {
            $query = $query->where('articles.price', '>=', $request->min_price);
        }
        if($request->max_price != null) {
            $query = $query->where('articles.price', '<=', $request->max_price);
        }

        $query = $query->whereDate('articles.start_date', '<=', date('Y-m-d'))
        ->whereDate('articles.end_date', '>=', date('Y-m-d'))
        ->where('articles.status',1)->orderBy('articles.pin', 'DESC')->orderBy('articles.created_at', 'DESC');
        $searchArticleAll = $query->get();
        $searchArticle = $query->paginate(3);

        return view('realestate.searchArticle.list',compact('searchArticle','searchArticleAll'));

    }
}
