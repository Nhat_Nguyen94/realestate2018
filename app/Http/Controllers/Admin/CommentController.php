<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Comment;

class CommentController extends Controller
{
    public function index(){
    	$comment = Comment::all();
    	return view('admin.comment.list',compact('comment'));
    }

    public function destroy($id){
    	$comment = Comment::find($id);
    	$comment->delete();
    	return redirect()->route('admin.comment.list');
    }
}
