<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\District;
use App\City;
use Illuminate\Support\Facades\DB;

class DistrictController extends Controller
{
      public function index() // this is list 
    {
    	$district = District::select();
      if(isset($_GET['search']) && strlen(trim($_GET['search'])) != 0)
      {
        $district = $district->where('name', 'LIKE', '%' . $_GET['search'] . '%')
                             ->orWhereHas('City', function($q)
                              {
                                $q->where('name', 'like',  '%' . $_GET['search'] . '%');
                              });   
      }
      $district = $district->paginate(3);
    	return view('admin.district.index',compact('district'));
    }

    public function index2($id = null){
    	$city = City::all();
      if(isset($id)){
        $name = District::find($id);
      }
   		return view('admin.district.created',compact('city','name'));
   }

   public function store(Request $request,$id = null){

   		    $this->validate($request,
        [
            'district' => 'required|min:2|max:255',
        ],
     	[
            'required' => 'District không được để trống',
            'min' => 'District không được nhỏ hơn :min',
            'max' => 'District không được lớn hơn :max',
        ]);
      if(isset($id)){
          $district = District::find($id);
      }else{
          $district = new District;
      }
   		$district->name = $request->district;
   		$district->city_id = $request->select_city;
   		$district->save();
   		 return redirect('admin/district');
   }

   public function destroy($id){
          $district = new District();
          DB::table('districts')->where('id',$id)->delete();
          return redirect('admin/district');
   }

   // public function searchBykeyword(Request $request){
   //    var_dump($request->all());
   //    exit;
   // }

   // public function update($id,Request $request){
   //    DB::table('districts')->where('id',$id)->update(array('name'=>$request->district,
   //      'city_id'=>$request->select_city));
   //     return redirect('admin/district');
   // }

}
