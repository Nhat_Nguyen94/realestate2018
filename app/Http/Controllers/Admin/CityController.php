<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\City;
use App\District;
use Illuminate\Support\Facades\DB;

class CityController extends Controller
{
    public function index() // this is list 
    {
        $city = City::select();
        if(isset($_GET['search']) && strlen(trim($_GET['search'])) != 0)
        {
             $city = $city->where('name', 'LIKE', '%' . $_GET['search'] . '%');
        }
        $city = $city->paginate(3);
    	return view('admin.city.index',compact('city'));
    }

      public function index2($id = null) 
    {   
        if(isset($id)){
        $name = City::find($id);
        }
    	return view('admin.city.created',compact('name'));
    }

    public function createOrupdate(Request $request,$id = null){
          $this->validate($request,
        [
            'city' => 'required',
        ],
        [
            'required' => 'City không được để trống',
        ]);
        if(isset($id)){
          $city = City::find($id);
        }else{
          $city = new City();
        }
        $city->name = $request->city;
        $city->save();
        return redirect('admin/city');
    }

    public function destroy($id){
        $city = City::find($id);
        $city->delete();
        return redirect()->route('admin.city.index');
    }
}
