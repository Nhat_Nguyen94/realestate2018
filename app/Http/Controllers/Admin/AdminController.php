<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Article;
use App\Category;
use App\City;
use App\District;
use App\Comment;
use App\User;
class AdminController extends Controller
{
    public function index(){
    	$articleHidden = Article::where('status',0)->count();
    	$comment = Comment::count();
        $userLock = User::where('level',3)->count();
    	$articleNew = Article::orderBy('created_at', 'desc')->paginate(4);
    	// dd($articleNew);
    	//dd($comment);
    	// dd($comment);
    	//dd($article);
		return view('admin.index',compact('articleHidden','comment','articleNew','userLock'));
    }
}
