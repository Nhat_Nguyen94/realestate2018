<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Validator;
class UserController extends Controller
{
    public function index(){
    	 $user = User::all();	
    	 return view('admin.user.list',compact('user'));
    }

    public function index2($id = null){
      if(isset($id)){
        $user = User::find($id);
      }
   		return view('admin.user.updated',compact('user'));
   	}

   	function store(Request $request, $id = null)
    {
    	// dd($id);
        // dd($request->all());
        $request->validate(
            [
                'name'  => 'required',
                'phone' => 'required',
            ],
            ['name.required' => 'Tên không được để trống',
                'phone.required' => 'SDT không được để trống',
            ]);
        $user = User::find($id);
        $user->name  = $request->name;
        $user->phone = $request->phone;
        $user->image = $request->image;
        $user->level = $request->level;
        $user->save();
        return redirect()->route('admin.user.list');
    }

    public function destroy($id = null){
    	// dd($id);
    	$user = User::find($id);
    	$user->delete();
    	return redirect()->route('admin.user.list');
    }
}
