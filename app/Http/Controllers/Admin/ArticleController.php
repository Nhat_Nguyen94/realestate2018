<?php

namespace App\Http\Controllers\Admin;

use App\Article;
use App\Category;
use App\City;
use App\District;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Auth;
use Illuminate\Support\Facades\DB;

class ArticleController extends Controller
{

    public function __construct()
    {
        Validator::extend('check_number', function ($attribute, $value, $parameters) {
            return $value > 0;
        });
    }

    public function index()
    {

        $article = Article::select();
        if (isset($_GET['search']) && strlen(trim($_GET['search'])) != 0) {
            $article = $article->where('title', 'LIKE', '%' . $_GET['search'] . '%');
        }
        $article = $article->orderBy('created_at', 'DESC')->paginate(4);
        return view('admin.article.list', compact('article'));
    }

    public function index2($id = null)
    {
        $category = Category::all();
        $city     = City::all();
        if (isset($id)) {
            $name = Article::find($id);
            // dd($name->images->first());
        }
        return view('admin.article.create', compact('category', 'city', 'name'));
    }

    public function indexchecklist(){
        $article = Article::where('status',0)->orderBy('created_at', 'ASC')->paginate(4);
        return view('admin.article.checklist',compact('article'));

    }

    public function updateChecklist($id){
        // dd($id);
        $article = Article::find($id);
        $article->status = 1  ;
        $article->save();
        return redirect()->route('admin.article.checklist')->with('update',1);
    }

    public function searchDistrict(Request $request)
    {
        $id       = $request->id;
        $district = new District();
        $dist     = $district->select()->where('city_id', $id)->get();
        return json_encode($dist);
    }

    public function store(Request $request, $id = null)
    {   
        // dd($request->all());
           $request->validate(
            [   
                'title' => 'required',
                'recommend' => 'required',
                'content' => 'required',
                'document' => 'required',
                'address' => 'required',
                'direction' => 'required',
                'bedroom' => 'required',
                'bathroom' => 'required',
                'area' => 'required|numeric',
                'price' => 'required|numeric|check_number',
                'image' => 'present',
                'file' => 'image',
                'file' => 'image',
            ],
            [   'title.required' => 'Tiêu đề không được để trống',
                'recommend.required' => 'Tóm tắt không được để trống',
                'document.required' => 'Giấy tờ không được để trống',
                'content.required' => 'Nội Dung không được để trống',
                'address.required' => 'Địa Chỉ không được để trống',
                'direction.required' => 'Hướng không được để trống',
                'area.required' => 'Diện tích không được để trống',
                'bedroom.required' => 'Số phòng ngủ không được để trống',
                'bathroom.required' => 'Số phòng tắm không được để trống',
                'price.check_number'=>'giá tiền phải là số dương',
                'area.numeric' => 'Diện tích phải là số ',
                'price.required' => 'Giá không được để trống',
                'price.numeric' => 'Giá phải là số ',
                'image.present' => 'Bài viết phải có ít nhất một hình',
                'file.image' => 'The file must be an image (jpeg, png, bmp, gif, or svg)',
            ]);
        if ($id) {
            $article = Article::find($id);
        } else {
            $article = new Article();
        }
        $article->title       = $request->title;
        $article->recommend   = $request->recommend;
        $article->content     = $request->content;
        $article->document    = $request->document;
        $article->address     = $request->address;
        $article->direction   = $request->direction;
        $article->price       = $request->price;
        $article->area        = $request->area;
        $article->start_date  = $request->start_date;
        $article->end_date    = $request->end_date;
        $article->status      = $request->status;
        $article->category_id = $request->select_category;
        $article->district_id = $request->select_district;
        if ($request->pin == 1) {
            $article->pin = 1;
        } else {
            $article->pin = 0;
        }
         if ($id) {
           $article->user_id = $article->user_id;
        }else{
            $article->user_id = Auth::user()->id;
        }
        
        $article->lat = $request->lat;
        $article->long = $request->long;
        $article->bedroom = $request->bedroom;
        $article->bathroom = $request->bathroom;
        $article->view    = 1;
        $article->save();
        // if($request->UpdateImageID){
        //     foreach ($request->UpdateImageID as $id) {
        //        $images =  $article->images()->find($id);
        //        // dd($images);
        //        foreach ($request->image as $value) {
        //             $images->src =  $value;
        //             $images->save();
        //        }          
        //     }
        // }else{
        //     foreach ($request->image as $val) {
        //     // dd($val);
        //     $dataImage['src'] = $val;
        //      $images = $article->images()->create($dataImage);
        //     } 
        // }
        if(isset($article->images[0]->src))
        {
            $idDetailArray = $article->images->pluck('id')->toArray();
            foreach ($idDetailArray as $value) {
                $article->images()->find($value)->delete();
            }
        }
         foreach ($request->image as $val){
              $dataImage['src'] = $val;
             $images = $article->images()->create($dataImage);
         }       
        return redirect()->route('admin.article.list');
    }

    public function destroy($id)
    {
        $article = Article::find($id);
        $article->delete();
        return redirect()->route('admin.article.list');
    }

    public function uploadImage($type = null, Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'file' => 'image',
            ],
            [
                'file.image' => 'The file must be an image (jpeg, png, bmp, gif, or svg)',
            ]);
        if ($validator->fails()) {
            return json_encode(array(
                'status' => false,
                'errors' => $validator->errors(),
            ));
        }
        $extension = $request->file('file')->getClientOriginalExtension();
        if ($type) {
            $dir = 'uploads/' . $type . '/';
        } else {
            $dir = 'uploads/';
        }
        $filename = uniqid() . '_' . time() . '.' . $extension;
        $request->file('file')->move($dir, $filename);
        $nameAndPatch = $dir . $filename;
        return json_encode(['status' => true, 'filename' => $nameAndPatch]);
    }


}
