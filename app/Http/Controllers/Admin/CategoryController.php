<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;

class CategoryController extends Controller
{
    public function index() // this is list 
    {
    	  $category = Category::select();
        if(isset($_GET['search']) && strlen(trim($_GET['search'])) != 0)
        {
             $category = $category->where('name', 'LIKE', '%' . $_GET['search'] . '%');
        }
        $category = $category->paginate(3);
    	return view('admin.category.list',compact('category'));
    }

    public function create($id = null) // show template create
    {
    	if(isset($id)){
        	$name = Category::find($id);
        }
    	return view('admin.category.create',compact('name'));
    }

    public function store(Request $request,$id = null) // to do create
	{
		if($id){
			$category = Category::find($id);
		}else{
			$category = new Category;
		}
		$category->name = $request->category;
		if($request->status == "1")
		{
			$category->status = 1;
		}
		else
		{
			$category->status = 0;
		}
		$category->save();
		return redirect()->route('admin.category.list');

	}

	public function show() // show details 
	{

	}

	public function edit($id) // show template edit
	{
	    if(isset($id)){
            $name = Category::find($id);
        }
        return view('AdminFront.category.add',compact('name'));
	}    

	public function update($id,Request $request) // to do update
	{
		// $category = Category::find($id)
		// $category->name = $request->name;
		// if($request->pin == "yes")
		// {
		// 	$category->pin = 1;
		// }
		// else
		// {
		// 	$category->pin = 0;
		// }
		// $category->save();
		// return redirect()->route('admin.category.index');
		
	}

	public function destroy($id)
	{
		$category = Category::find($id);
		$category->delete();
		return redirect()->route('admin.category.list');
	}
}
