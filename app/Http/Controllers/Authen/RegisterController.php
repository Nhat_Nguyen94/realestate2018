<?php

namespace App\Http\Controllers\Authen;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\User;
class RegisterController extends Controller
{
    public function getRegister(){
    	return view('authen.register');
    }

    public function postRegister(Request $request){
    	
  		$this->validate($request,
        [
            'name' => 'required|min:2|max:255',
            'email' => 'required|email|unique:users',
            'phone' => 'required|min:10',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required',
            'terms' => 'required',
        ],
        [
            'name.required' => 'Tên không được để trống',
            'email.required' => 'Email không được để trống',
            'email.email' => 'Email không đúng định dạng ',
            'email.unique' => 'Email đã tồn tại,Vui lòng nhập Email khác',
            'phone.required' => 'Số điện thoại không được để trống',
            'phone.min' => 'Số điện thoại phải có tối thiểu 10 số',
            'password.required' => 'Mật khẩu không được để trống',
            'password.confirmed' => 'Mật khẩu và xác nhận mật khẩu không trùng nhau.',
            'password_confirmation.required' => 'Xác nhận Mật khẩu không được để trống',
            'terms.required' => 'Bạn phải đồng ý với điều khoản',
        ]);
		$user = new User;
		$user->name = $request->name;
		$user->email = $request->email;
		$user->phone = $request->phone;
		$user->level  = 1 ;
		$user->password =  bcrypt($request->password);
		$user->save();
      	return redirect()->route('authen.login')->with('status',1);
    }
}
