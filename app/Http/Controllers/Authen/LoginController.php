<?php

namespace App\Http\Controllers\Authen;

use App\Article;
use App\Category;
use App\City;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Illuminate\Http\Request;
use Validator;

class LoginController extends Controller
{

    public function __construct()
    {
        Validator::extend('checkuser', function ($attribute, $value, $parameters) {

            $user = User::where('email', '=', $parameters[0])->first();
            if ($user) {
                $hasher = app('hash');
                if ($hasher->check($value, $user->password)) {
                    return true;
                }
            }
            return false;
        });
    }

    public function getLogin()
    {

        $category = Category::select()->where('status', 1)->get();
        $city     = City::all();
        $article  = Article::select()->where('status', 1)->get();
        return view('authen.login', compact('category', 'city'));
    }
    public function postLogin(Request $request)
    {
            // dd($request->all());
        //    Validator::extend('pwdvalidation', function ($field, $value, $parameters) {
        //     return Hash::check($value, Auth::user()->password);
        // });

        $this->validate($request,
            [
                'email'    => 'required|email',
                'password' => 'required|checkuser:' . $request->email,
            ],
            [
                'email.required'     => 'Email không được để trống',
                'email.email'        => 'Email không đúng định dạng ',
                'password.required'  => 'Mật khẩu không được để trống',
                'password.checkuser' => 'Mật khẩu hoặc email không chính xác.',
            ]);
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            if (Auth::user()->level == 1) {
                return redirect()->route('realestate.index');
            } elseif (Auth::user()->level == 2) {
                return redirect()->route('admin.index');
            } elseif (Auth::user()->level == 3){
                Auth::logout();
                return redirect()->route('authen.login')->with('status1',6);
            }
        }
    }
    public function logout()
    {
        Auth::logout();
        return redirect()->route('realestate.index');
    }
}
