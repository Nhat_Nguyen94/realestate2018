<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Validator;
use Hash;
use Auth;
use App\Category;
use App\City;
use App\Article;
use App\Comment;
use App\District;
use App\Contact;
use Illuminate\Support\Facades\DB;
use Mail;
use App\Mail\OrderShipped;
use Carbon\Carbon;

class UserController extends Controller
{

  
   public function __construct()
    {
        Validator::extend('check_number', function ($attribute, $value, $parameters) {
            return $value > 0;
        });
    }
    function index()
    {
        return view('realestate.users.profile');
    }

    function uploadImage($type = null, Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'file' => 'image',
            ],
            [
                'file.image' => 'The file must be an image (jpeg, png, bmp, gif, or svg)',
            ]);
        if ($validator->fails()) {
            return json_encode(array(
                'status' => false,
                'errors' => $validator->errors(),
            ));
        }
        $extension = $request->file('file')->getClientOriginalExtension();
        if ($type) {
            $dir = 'uploads/' . $type . '/';
        } else {
            $dir = 'uploads/';
        }
        $filename = uniqid() . '_' . time() . '.' . $extension;
        $request->file('file')->move($dir, $filename);
        $nameAndPatch = $dir . $filename;
        return json_encode(['status' => true, 'filename' => $nameAndPatch]);
    }

    function store(Request $request, $id = null)
    {
        // dd($request->all())
        $request->validate(
            [
                'name'  => 'required',
                'phone' => 'required',
            ],
            ['name.required' => 'Tên không được để trống',
                'phone.required' => 'SDT không được để trống',
            ]);
        if ($id) {
            $user = User::find($id);
        }
        $user->name  = $request->name;
        $user->phone = $request->phone;
        $user->image = $request->image;
        $user->save();
        return redirect()->route('user.index')->with('status', 1);
    }

    function changePasswordindex()
    {
        return view('realestate.users.changepassword');
    }

    function changePassword(Request $request)
    {
          Validator::extend('pwdvalidation', function ($field, $value, $parameters) {
            return Hash::check($value, Auth::user()->password);
        });
        // dd($request->all());
        // return view('realestate.users.changepassword');
        $request->validate([
            'password_current'      => 'required|pwdvalidation',
            'password_new'          => 'required|confirmed|min:6|max:50|different:password_current',
            'password_new_confirmation' => 'required',
        ], [
            'password_current.required'      => 'Mật khẩu cũ không được để trống.',
            'password_current.pwdvalidation' => 'Mật khẩu cũ không đúng.',
            'password_new.required'          => 'Mật khẩu mới không được để trống.',
            'password_new.confirmed'         => 'Mật khẩu mới và xác nhận mật khẩu không trùng khớp .',
            'password_new.min'               => 'Mật khẩu mới phải có tối thiểu 6 ký tự.',
            'password_new.max'               => 'Mật khẩu mới chỉ được nhập tối đa 50 ký tự.',
            'password_new.different'         => 'Mật khẩu mới và mật khẩu cũ không được giống nhau.',
            'password_new_confirmation.required'          => 'Xác nhận mật khẩu mới không được để trống.',
        ]);

        $id             = Auth::user()->id;
        $user           = User::find($id);
        $user->password = bcrypt($request->password_new);
        $user->save();
        Auth::logout();
        return redirect()->route('authen.login');

    }


    public function getArticle(){
        $category = Category::all();
        $city     = City::all();
        return view('realestate.users.createdarticle',compact('category', 'city'));
    }

    public function postArticle(Request $request){
        // dd($request->all());
             $request->validate(
            [   
                'title' => 'required',
                'recommend' => 'required',
                'content123' => 'required',
                'document' => 'required',
                'address' => 'required',
                'direction' => 'required',
                 'bedroom' => 'required',
                'bathroom' => 'required',
                'area' => 'required|numeric',
                'price' => 'required|numeric|check_number',
                'image' => 'present',
                'file' => 'image',
                'file' => 'image',
            ],
            [   'title.required' => 'Tiêu đề không được để trống',
                'recommend.required' => 'Tóm tắt không được để trống',
                'document.required' => 'Giấy tờ không được để trống',
                'content123.required' => 'Nội Dung không được để trống',
                'address.required' => 'Nội Dung không được để trống',
                'direction.required' => 'Hướng không được để trống',
                 'bedroom.required' => 'Số phòng ngủ không được để trống',
                'bathroom.required' => 'Số phòng tắm không được để trống',
                'area.required' => 'Diện tích không được để trống',
                'price.check_number'=>'giá tiền phải là số dương',
                'area.numeric' => 'Diện tích phải là số ',
                'price.required' => 'Giá không được để trống',
                'price.numeric' => 'Giá phải là số ',
                'image.present' => 'Bài viết phải có ít nhất một hình',
                'file.image' => 'The file must be an image (jpeg, png, bmp, gif, or svg)',
            ]);
        
        $article = new Article();
        $article->title       = $request->title;
        $article->recommend   = $request->recommend;
        $article->content     = $request->content123;
        $article->document    = $request->document;
        $article->address     = $request->address;
        $article->direction   = $request->direction;
        $article->price       = $request->price;
        $article->area        = $request->area;
        $article->start_date  = $request->start_date;
        $article->end_date    = $request->end_date;
        $article->status      = $request->status;
        $article->category_id = $request->select_category;
        $article->district_id = $request->select_district;
        $article->pin = 0;
        $article->user_id = Auth::user()->id;
        $article->view    = 1;  
        $article->bedroom    = $request->bedroom;  
        $article->bathroom    = $request->bathroom;
        $article->lat    = $request->lat;
        $article->long    = $request->long;
        $article->status  = 0;  
        $article->save();
        if(isset($article->images[0]->src))
        {
            $idDetailArray = $article->images->pluck('id')->toArray();
            foreach ($idDetailArray as $value) {
                $article->images()->find($value)->delete();
            }
        }
         foreach ($request->image as $val){
              $dataImage['src'] = $val;
             $images = $article->images()->create($dataImage);
         }
         return redirect()->route('user.getarticle')->with('created',3);       
    }
    public function listArticle(){
        $article = Article::where('user_id',Auth::user()->id)->paginate(3);
        return view('realestate.users.list',compact('article')) ;   
    }

    public function indexComment(){
        return view('realestate.users.comment');
    }

    public function createdComment(Request $request){
         $request->validate(
            [   
                'title' => 'required|min:3',
                'content' => 'required|min:10',
            ],[
                 'title.required' => 'Tiêu đề không được để trống',
                 'content.required' => 'Nội Dung không được để trống',
                 'title.min' => 'Tiêu đề tối thiểu phải có 3 ký tự',
                 'content.min' => 'Nội Dung tối thiểu phải có 10 ký tự',
            ]);
        $comment = new Comment;
        $comment->title = $request->title;
        $comment->content = $request->content;
        $comment->user_id = Auth::user()->id;
        $comment->save();
        return redirect()->route('user.commentindex')->with('status',13);
    }

       public function searchDistrict(Request $request)
    {
        $id       = $request->id;
        $district = new District();
        $dist     = $district->select()->where('city_id', $id)->get();
        return json_encode($dist);
    }

    public function addcontact(Request $request){
        $contact = new Contact();
        $contact->content  = $request->content_contact;
        $contact->user_from  = $request->user_from;
        $contact->user_to  = $request->user_to;
        $contact->article_id  = $request->article_id;
        $contact->status  = 1;
        $contact->save();

        $id = $request->user_to; // điền 1 mã id bất kỳ của user trong bảng users 
        $user = User::find($id);
         Mail::send('emails.sendemail', ['user' => $user], function ($m) use ($user) {
            $m->from('admin@gmail.com', 'Xin Chào '.$user->name);

            $m->to($user->email, $user->name)->subject('Thông báo từ website Bất động sản của Nhật');
        });
        return redirect()->route('realestate.index');
    }

    public function listcontact(){
        // dd(Auth::user()->id);
        $contact = Contact::where('user_to',"=",Auth::user()->id)->get();
        // dd($contact);
        // dd($contact);
        // foreach ($contact as $value) {
        //     dd($value->content);
        //     $user_from = User::find($value->user_from);
        //     $article = Article::find($value->article_id);
        //     dd( $article);
        // }
        return view('realestate.users.listcontact',compact('contact'));
       


    }

}
