<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
// Route::get('/test','testController@getData');
// Route::any('add','testController@store')->name('test.store');



    
Route::get('/send_email', array('uses' => 'EmailController@sendEmailReminder'));
Route::get('/googlemap', function(){
    return view('testmap');
});

Route::get("/email", function() {
   Mail::raw('Now I know how to send emails with Laravel', function($message)
    {
        $message->subject('Hi There!!');
        $message->from('nhat.nguyenminh94@gmail.com');
        $message->to('tieuviem5594@gmail.com');
    });
});

Route::get('/', 'Realestate\ArticlesController@index')->name('realestate.index');
Route::post('/search', 'Realestate\ArticlesController@searchDistrict')->name('realestate.searchdistrict');
Route::get('/cate/{id}', 'Realestate\ArticlesController@sortByCategory')->name('realestate.sortcategory');
Route::get('detail/{id}', 'Realestate\ArticlesController@detail')->name('realestate.detail');
Route::get('/searchArticle', 'Realestate\ArticlesController@searchArticle')->name('realestate.search');
Route::get('/contactus',function(){
    return view('realestate.about.contactus');
});
// login
Route::get('/login', 'Authen\LoginController@getLogin')->name('authen.login');
Route::post('/login/', 'Authen\LoginController@postLogin')->name('authen.postlogin');
Route::get('/logout', 'Authen\LoginController@logout')->name('authen.logout');

//register
Route::get('/register', 'Authen\RegisterController@getRegister')->name('authen.register');
Route::post('/register/', 'Authen\RegisterController@postRegister')->name('authen.postregister');

// Route::get('duong dan','\controller@function') //App\Http\Controllers
// App\Http\Controllers\Admin
Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => ['admin']], function () {
    Route::get('/', 'AdminController@index')->name('admin.index')->middleware('admin');
    Route::group(['prefix' => 'category'], function () {
        Route::get('/', 'CategoryController@index')->name('admin.category.list');
        Route::get('create', 'CategoryController@create')->name('admin.category.create');
        Route::post('/', 'CategoryController@store')->name('admin.category.store');
        Route::get('edit/{id}', 'CategoryController@create')->name('admin.category.edit');
        Route::post('/{id}', 'CategoryController@store')->name('admin.category.update');
        Route::get('/{id}', 'CategoryController@destroy')->name('admin.category.destroy');
    });
    // city
    Route::group(['prefix' => 'city'], function () {
        Route::get('/', 'CityController@index')->name('admin.city.index');
        Route::get('created', 'CityController@index2')->name('admin.city.created');
        Route::get('edit/{id}', 'CityController@index2')->name('admin.city.edit');
        Route::get('/{id}', 'CityController@destroy')->name('admin.city.destroy');
        Route::post('create', 'CityController@createOrupdate')->name('admin.city.create');
        Route::post('update/{id}', 'CityController@createOrupdate')->name('admin.city.update');
    });

    //district
    Route::group(['prefix' => 'district'], function () {
        Route::get('/', 'DistrictController@index')->name('admin.district.index');
        Route::get('created', 'DistrictController@index2')->name('admin.district.created');
        Route::post('/', 'DistrictController@store')->name('admin.district.store');
        Route::get('/{id}', 'DistrictController@destroy')->name('admin.district.destroy');
        Route::get('edit/{id}', 'DistrictController@index2')->name('admin.district.edit');
        Route::post('/{id}', 'DistrictController@store')->name('admin.district.update');

    });
    //Article
    Route::group(['prefix' => 'article'], function () {
        Route::get('/', 'ArticleController@index')->name('admin.article.list');
        Route::get('/checklist', 'ArticleController@indexchecklist')->name('admin.article.checklist');
        Route::post('/search', 'ArticleController@searchDistrict')->name('admin.article.searchdistrict');
         Route::get('/updatechecklist/{id}', 'ArticleController@updateChecklist')->name('admin.article.updatechecklist');
        Route::get('created', 'ArticleController@index2')->name('admin.article.created');
        Route::post('uploadimage/{type?}', 'ArticleController@uploadImage')->name('admin.article.uploadimage');
        Route::post('/', 'ArticleController@store')->name('admin.article.store');
        Route::get('/{id}', 'ArticleController@destroy')->name('admin.article.destroy');
        Route::get('edit/{id}', 'ArticleController@index2')->name('admin.article.edit');
        Route::post('/{id}', 'ArticleController@store')->name('admin.article.update');
    });
    //User
     Route::group(['prefix' => 'user'], function () {
            Route::get('/', 'UserController@index')->name('admin.user.list');
            Route::get('/{id}', 'UserController@destroy')->name('admin.user.destroy');
            Route::get('edit/{id}', 'UserController@index2')->name('admin.user.edit');
            Route::post('/{id}', 'UserController@store')->name('admin.user.update');
     });

     // Comment 
     Route::group(['prefix' => 'comment'], function () {
            Route::get('/', 'CommentController@index')->name('admin.comment.list');
            Route::get('/{id}', 'CommentController@destroy')->name('admin.comment.destroy');

     });

});


Route::group(['prefix' => 'user', 'namespace' => 'User','middleware' => ['user']], function () {
     Route::get('/', 'UserController@index')->name('user.index');
     Route::post('uploadimage/{type?}', 'UserController@uploadImage')->name('user.uploadimage');
     Route::post('/{id}', 'UserController@store')->name('user.update');
     Route::get('/changepassword', 'UserController@changePasswordindex')->name('user.passwordindex');
     Route::put('/changepassword', 'UserController@changePassword')->name('user.changepassword');
     Route::get('getArticle', 'UserController@getArticle')->name('user.getarticle');
     Route::any('postArticle/', 'UserController@postArticle')->name('user.postarticle');
     Route::get('listArticle', 'UserController@listArticle')->name('user.listarticle');
     Route::get('comment', 'UserController@indexComment')->name('user.commentindex');
     Route::post('createdComment/comment/', 'UserController@createdComment')->name('user.createdcomment');
    Route::post('/search/city/districts', 'UserController@searchDistrict')->name('user.city.searchDistrict');
    Route::get('contact/addcontact', 'UserController@addcontact')->name('user.contact.addcontact');
    Route::get('contact/listcontact', 'UserController@listcontact')->name('user.contact.listcontact');


});