@extends('realestate.layouts.master')

@section('content')
<div id="main">
<div class="container">
    <div class="row justify-content-md-center">
          <div class="col col-md-12 col-lg-10 col-xl-8">
        <nav aria-label="breadcrumb">
           <!--  <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item"><a href="#">Account</a></li>
            <li class="breadcrumb-item active" aria-current="page">Login</li>
            </ol> -->
            </nav>
        <div class="page-header">
        <h1>Vui lòng đăng nhập hoặc đăng ký</h1>
        </div>
      </div>
    </div>
  </div>
<div id="content">
  <div class="container">
    <div class="row justify-content-md-center align-items-center">
      <div class="col col-md-6  col-lg-5 col-xl-4">
        <ul class="nav nav-tabs tab-lg" role="tablist">
          <li role="presentation" class="nav-item"><a class="nav-link active" href="{{route('authen.login')}}#form_login">Đăng Nhập</a></li>
          <li role="presentation" class="nav-item"><a class="nav-link" href="{{route('authen.register')}}#form_register">Đăng ký</a></li>
        </ul>
          @if(session('status1') == 6)
                <div class="alert alert-danger" role="alert">
               Tài khoản đã bị khóa vui lòng liên hệ Quản trị viên để mở khóa.
                </div>
          @endif
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="login">
            <form method="post" action="{{route('authen.postlogin')}}" id="form_login">
              @csrf
              @if(session('status') == 1)
                <div class="alert alert-success" role="alert">
                Đăng ký thành công bạn có thể Đăng nhập.
                </div>
              @endif
              @if($errors->any())
                @foreach($errors->all() as $error)
                <div class="alert alert-danger" role="alert">
                {{$error}}
                </div>
                @endforeach
              @endif
              <div class="form-group">
                <label for="email">Email address</label>
                <input type="email" id="email" name="email" class="form-control form-control-lg" placeholder="Email">
              </div>
              <div class="form-group">
                <label for="password">Mật khẩu</label>
                <input type="password" id="password" name="password" class="form-control form-control-lg" placeholder="Mật Khẩu">
              </div>
         <!--      <p class="text-lg-right"><a href="forgot-password.html">Forgot Password</a></p> -->
            <!--   <div class="checkbox">
                <input type="checkbox" id="remember_me">
                <label for="remember_me">Remember Me</label>
              </div> -->
              <button type="submit" class="btn btn-primary btn-lg">Đăng Nhập</button>
            </form>
          </div>
        </div>
        <div> </div>
      </div>

 <!--      <div class="col-md-6 col-lg-5 col-xl-4">
        <div class="socal-login-buttons"> <a href="#" class="btn btn-social btn-block btn-facebook"><i class="icon fa fa-facebook"></i> Continue with Facebook</a> <a href="#" class="btn btn-social btn-block btn-twitter"><i class="icon fa fa-twitter"></i> Continue with Twitter</a> <a href="#" class="btn btn-social btn-block btn-google"><i class="icon fa fa-google"></i> Continue with Google</a> </div>
      </div> -->
    </div>
  </div>
</div>
@endsection
