@extends('admin.layouts.master')

@section('content')
	<div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Tin Đăng</a>
        </li>
      
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Bảng Tin Đăng</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <div class="col-md-12 search_add">
               <div class="row" >
                <div class="col-md-10 group_search">
                  <form class="form-inline"  >
                    <input class="form-control col-md-4" name="search" value="{{isset($_GET['search'])?$_GET['search']:''}}" type="search" placeholder="Tìm kiếm" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0 btn_search" type="submit">Tìm Kiếm</button>
                  </form>
                </div>
                <div class="col-md-2 group_add">
                 <a class="btn btn-outline-success " href="{{ route('admin.article.created')}}" role="button">Thêm mới</a>
               </div>                       
             </div>
           </div>
              <thead>
              	<tr>
              		<th>STT</th>
                  <th>ID Tin Đăng </th>
              		<th>Tiêu đề</th>
              		<th>Tóm Tắt</th>
              		<th>Thể Loại</th>
                  <th>User</th>
                  <th>Quận Huyện</th>
                  <th>Trạng Thái</th>
                  <th class="btn_delete">Công cụ</th>
                  <th class="btn_update">Công cụ</th>
              	</tr>
              </thead>
                 @foreach($article as $key => $value)
                <tr>
                
                <td>{{$key+1}}</td>
                <td>{{$value->id}}</td>
                <td>{{$value->title}}</td>
                <td>{{$value->recommend}}</td>
                <td>{{$value->category->name}}</td>
                <td>{{$value->user_id}} </td>
                <td>{{$value->district->name}}</td>
                <td>@if($value->status == '1' )
                    Hiện
                    @else
                    Ẩn
                    @endif
                </td>
                <td class="btn_delete"><a class="btn btn-outline-danger" href="{{route('admin.article.destroy',['id'=>$value->id])}}">Xóa</a></td>
                <td class="btn_update"><a  class="btn btn-outline-info" href="{{route('admin.article.edit',['id'=>$value->id])}}">Cập Nhật/Duyệt</a></td>
              
                </tr>
                 @endforeach
            </table>
          </div>
          <div class="row justify-content-md-center" >          
              {!! $article->links('paginate') !!}
        </div>
        </div>
        <!-- <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div> -->
      </div>
    </div>
@endsection