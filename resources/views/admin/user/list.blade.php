@extends('admin.layouts.master')

@section('content')
<div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Quản lý User</a>
        </li>

      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Bảng User</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <div class="col-md-12 search_add">
               <div class="row" >
           <!--      <div class="col-md-10 group_search">
                  <form class="form-inline"  >
                    <input class="form-control col-md-4" name="search" value="{{isset($_GET['search'])?$_GET['search']:''}}" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0 btn_search" type="submit">Search</button>
                  </form>
                </div> -->
                <!-- <div class="col-md-2 group_add">
                 <a class="btn btn-outline-success " href="{{ route('admin.article.created')}}" role="button">Add New</a>
               </div>       -->
             </div>
           </div>
              <thead>
              	<tr>
              		<th>STT</th>
                  	<th>User ID</th>
              		<th>Tên </th>
              		<th>Email</th>
                    <th>Hình Ảnh</th>
                    <th>Level</th>
                  <th class="btn_delete">Công Cụ</th>
                  <th class="btn_update">Công Cụ</th>
              	</tr>
              </thead>
                   @foreach($user as $key => $value)
                <tr>
                <td>{{$key+1}}</td>
                <td>{{$value->id}}</td>
                <td>{{$value->name}}</td>
                <td>{{$value->email}}</td>
                <td style="text-align: center;"><img src="{{$value->image}}" alt="" style="width: 143px;height: 143px;"></td>
                <td>@if($value->level == '1' )
                    User
                    @elseif($value->level == '2' )
                    Admin
                    @else
                    Tài khoản bị khóa
                    @endif
                </td>
                <td class="btn_delete"><a class="btn btn-outline-danger" href="{{route('admin.user.destroy',['id'=>$value->id])}}">Xóa </a></td>
                <td class="btn_update"><a  class="btn btn-outline-info" href="{{route('admin.user.edit',['id'=>$value->id])}}">Cập Nhật</a></td>
                </tr>
                 @endforeach
            </table>
          </div>

        </div>
        <!-- <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div> -->
      </div>
    </div>
@endsection
