@extends('admin.layouts.master')
@section('style')
  <link href="/admin-css/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="/admin-css/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="/admin-css/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="/admin-css/css/sb-admin.css" rel="stylesheet">
@endsection
@section('content')
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="{{route('admin.index')}}">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Tables</li>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i>Cập Nhật/Phân Quyền User</div>
        <div class="card-body">
    <form action="{{route('admin.user.update',['id'=>$user->id])}}" method="POST">
      <input type="hidden" name="_token" value="{{ csrf_token()}}">
      
    <div class="form-group">
      <label for="exampleInputEmail1">Name</label>
      <input type="text" name="name" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nhập User Name" value="{{isset($user->name) ? $user->name : '' }}">
    </div>
    <div class="form-group">
      <label for="exampleInputEmail1">Phone</label>
      <input type="text" name="phone" class="form-control" id="exampleInputEmail2" aria-describedby="emailHelp" placeholder="Nhập số điện thoại" value="{{isset($user->phone) ? $user->phone : '' }}">
    </div>
    <div class="form-group">
      <label for="exampleInputEmail1">Email</label>
      <input type="text" name="Email" class="form-control" id="exampleInputEmail3" aria-describedby="emailHelp" placeholder="Nhập số điện thoại" disabled value="{{isset($user->email) ? $user->email : '' }}">
    </div>
    <div class="col-md-12">
      <div class="row"> 
        <label>Hình Ảnh</label>
        <div class="col col-md-4">
          <label for="file-input" class=" form-control-label">File input</label>
          <input type="file" id="file-input" name="image_upload filesTest" class="form-control-file upload-image" onchange="UploadImage(this);">
        </div>
        <div class="col col-md-6">                     
         <div class="media-left"> <a href="agent.html">
          <img class="media-object rounded-circle" name="image" id="image_show" src="{{isset($user->image) ? $user->image : '/realestate/img/demo/Screenshot_2.png'}}" width="64" height="64" alt=""> 
          <input type="hidden" name="image" id="images_show_data" value="{{isset($user->image) ? $user->image : '/realestate/img/demo/Screenshot_2.png'}}"></a>
        </div>
      </div>
    </div>
  </div>
    <div class="form-group">
     <fieldset class="form-group">
      <legend>Quyền</legend>
      <div class="form-check">
        <label class="form-check-label">
          <input type="radio" class="form-check-input" name="level" id="optionsRadios1" value="1"
          @if(isset($user) && $user->level == 1 )
            checked
          @endif
           >
         User
        </label>
      </div>
      <div class="form-check">
        <label class="form-check-label">
          <input type="radio" class="form-check-input" name="level" id="optionsRadios2" value="2" 
          @if( isset($user) && $user->level == 2 )
            checked
          @endif
          >
         Admin
        </label>
      </div>
       <div class="form-check">
        <label class="form-check-label">
          <input type="radio" class="form-check-input" name="level" id="optionsRadios2" value="3" 
          @if( isset($user) && $user->level == 3 )
            checked
          @endif
          >
         Tài Khoản Bị Khóa
        </label>
      </div>
    </fieldset>
    </div>
   
    <button type="submit" class="btn btn-primary">Submit</button>
    </form>
        </div>
        <!-- <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div> -->
      </div>
    </div>
@endsection
@section('script')
    <!-- Bootstrap core JavaScript-->
    <script src="/admin-css/vendor/jquery/jquery.min.js"></script>
    <script src="/admin-css/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="/admin-css/vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="/admin-css/vendor/datatables/jquery.dataTables.js"></script>
    <script src="/admin-css/vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="/admin-css/js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="/admin-css/js/sb-admin-datatables.min.js"></script>
    <script>
    function UploadImage(e){
  
    var form_data = new FormData();
        form_data.append('file', e.files[0]);
        form_data.append('_token', '{{csrf_token()}}');
        $.ajax({
        url: "{{route('user.uploadimage',['type'=>'user'])}}", // Url to which the request is send
        data: form_data,
        type: 'POST',
        contentType: false,
        processData: false,
        success: function(res)   // A function to be called if request succeeds
        {
          dataSuccess = JSON.parse(res);
          // console.log(res);
          console.log(dataSuccess.status);
          if(dataSuccess.status == false){
            alert(dataSuccess.errors.file);
          }else{
          $('#image_show').attr('src','');
          $('#images_show_data').val('');
          console.log(dataSuccess.filename);
          $('#image_show').attr('src', '/'+dataSuccess.filename);
          $('#images_show_data').val('/'+dataSuccess.filename);
          }
         
        },error: function(error){
            alert(error);
            }
        });
        };

</script>  
@endsection