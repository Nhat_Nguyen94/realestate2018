@extends('admin.layouts.master')
@section('style')
<!--   <link href="../admin-css/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet"> -->
  <!-- Custom fonts for this template-->
 <!--  <link href="../admin-css/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"> -->
  <!-- Page level plugin CSS-->
<!--   <link href="../admin-css/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet"> -->
  <!-- Custom styles for this template-->
  <!-- <link href="../admin-css/css/sb-admin.css" rel="stylesheet"> -->
@endsection
@section('content')
     <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Tỉnh Thành</a>
        </li>
      
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
      <!--     <i class="fa fa-table"></i> Data Table Example</div>
 -->        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">

            <div class="col-md-12 search_add">
               <div class="row" >
                <div class="col-md-10 group_search">
                  <form class="form-inline"  >
                    <input class="form-control col-md-4" name="search" value="{{isset($_GET['search'])?$_GET['search']:''}}" type="search" placeholder="Tìm kiếm" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0 btn_search" type="submit">Tìm Kiếm</button>
                  </form>
                </div>
                <!--   <a href="{{ route('admin.district.created')}}" style="display: inline-block; float: left">Add New</a> -->
                <div class="col-md-2 group_add">
                 <a class="btn btn-outline-success " href="{{route('admin.city.created')}}" role="button">Thêm Mới</a>
               </div>                       
            </div>
           </div>
              <thead>
                <tr>
                  <th>STT</th>
                  <th>ID</th>
                  <th>Tên Tỉnh Thành</th>
                  <th class="btn_delete">Công Cụ</th>
                  <th class="btn_update">Công Cụ</th>
                </tr>
              </thead>
              @foreach($city as $key=>$val)
              <tbody>
                <tr>
                  <td>{{$key+1}}</td>
                  <td>{{$val->id}}</td>
                  <td>{{$val->name}}</td>
                  <td class="btn_delete">
                    @if(!isset($val->district[0]->id))
                    <a class="btn btn-outline-danger" href="{{route('admin.city.destroy',['id'=>$val->id])}}"
                    >Xóa</a>
                    @endif</td>
                  <td class="btn_update" ><a class="btn btn-outline-info" href="{{route('admin.city.edit',['id'=>$val->id])}}">Cập Nhật</a></td>
                </tr>
              </tbody>
           @endforeach
            </table>
          </div>
             <div class="row justify-content-md-center" >
          
              {!! $city->links() !!}
       
         
        </div>
        </div>
        <!-- <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div> -->
      </div>
    </div>


@endsection
@section('script')
    <!-- Bootstrap core JavaScript-->
<!--     <script src="../admin-css/vendor/jquery/jquery.min.js"></script>
    <script src="../admin-css/vendor/bootstrap/js/bootstrap.bundle.min.js"></script> -->
    <!-- Core plugin JavaScript-->
 <!--    <script src="../admin-css/vendor/jquery-easing/jquery.easing.min.js"></script> -->
    <!-- Page level plugin JavaScript-->
<!--     <script src="../admin-css/vendor/datatables/jquery.dataTables.js"></script>
    <script src="../admin-css/vendor/datatables/dataTables.bootstrap4.js"></script> -->
    <!-- Custom scripts for all pages-->
  <!--   <script src="../admin-css/js/sb-admin.min.js"></script> -->
    <!-- Custom scripts for this page-->
   <!--  <script src="../admin-css/js/sb-admin-datatables.min.js"></script> -->
@endsection