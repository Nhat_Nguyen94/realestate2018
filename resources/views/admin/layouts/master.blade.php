<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Admin-Page</title>
  <!-- Bootstrap core CSS-->
  <link href="/admin-css/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="/admin-css/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
<!--   <link href="/admin-css/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet"> -->
  <!-- Custom styles for this template-->
  <link href="/admin-css/css/sb-admin.css" rel="stylesheet">
  <link href="/admin-css/css/main.css" rel="stylesheet">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  @yield('style')
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
    @include('.admin.layouts.navmenu')
  <div class="content-wrapper">
    @yield('content')
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    @include('.admin.layouts.footer')  
  </div>
</body>
  <!-- Bootstrap core JavaScript-->
    <script src="/admin-css/vendor/jquery/jquery.min.js"></script>
    <script src="/admin-css/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="/admin-css/vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
<!--     <script src="/admin-css/vendor/chart.js/Chart.min.js"></script> -->
 <!--    <script src="/admin-css/vendor/datatables/jquery.dataTables.js"></script>
    <script src="/admin-css/vendor/datatables/dataTables.bootstrap4.js"></script> -->
    <!-- Custom scripts for all pages-->
    <script src="/admin-css/js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
  <!--   <script src="/admin-css/js/sb-admin-datatables.min.js"></script> -->
    <script>
    $.ajaxSetup({
    headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });
    </script>
   
    @yield('script')
</html>
