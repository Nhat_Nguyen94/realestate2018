@extends('admin.layouts.master')

@section('content')
   <div class="container-fluid">
      <!-- Breadcrumbs-->
      <!-- <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">My Dashboard</li>
      </ol> -->
      <!-- Icon Cards-->
      <div class="row">
        <div class="col-xl-4 col-sm-6 mb-4">
          <div class="card text-white bg-primary o-hidden h-100">
            <div class="card-body">
              <div class="card-body-icon">
                <i class="fa fa-fw fa-comments"></i>
              </div>
              <div class="mr-5">{{$articleHidden}} Tin đang chờ duyệt</div>
            </div>
            <a class="card-footer text-white clearfix small z-1" href="{{route('admin.article.checklist')}}">
              <span class="float-left">Xem chi tiết</span>
              <span class="float-right">
                <i class="fa fa-angle-right"></i>
              </span>
            </a>
          </div>
        </div>
        <div class="col-xl-4 col-sm-6 mb-4">
          <div class="card text-white bg-warning o-hidden h-100">
            <div class="card-body">
              <div class="card-body-icon">
                <i class="fa fa-fw fa-list"></i>
              </div>
              <div class="mr-5">Có {{$comment}} Góp ý</div>
            </div>
            <a class="card-footer text-white clearfix small z-1" href="{{route('admin.comment.list')}}">
              <span class="float-left">Xem chi tiết</span>
              <span class="float-right">
                <i class="fa fa-angle-right"></i>
              </span>
            </a>
          </div>
        </div>
 <!--        <div class="col-xl-3 col-sm-6 mb-3">
          <div class="card text-white bg-success o-hidden h-100">
            <div class="card-body">
              <div class="card-body-icon">
                <i class="fa fa-fw fa-shopping-cart"></i>
              </div>
              <div class="mr-5">123 New Orders!</div>
            </div>
            <a class="card-footer text-white clearfix small z-1" href="#">
              <span class="float-left">View Details</span>
              <span class="float-right">
                <i class="fa fa-angle-right"></i>
              </span>
            </a>
          </div>
        </div> -->
        <div class="col-xl-4 col-sm-6 mb-4">
          <div class="card text-white bg-danger o-hidden h-100">
            <div class="card-body">
              <div class="card-body-icon">
                <i class="fa fa-fw fa-support"></i>
              </div>
              <div class="mr-5">{{$userLock}} Tài khoản bị khóa</div>
            </div>
            <a class="card-footer text-white clearfix small z-1" href="{{route('admin.user.list')}}">
              <span class="float-left">Xem chi tiết</span>
              <span class="float-right">
                <i class="fa fa-angle-right"></i>
              </span>
            </a>
          </div>
        </div>
      </div>
      </div>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Các Bài Viết Mới Đăng</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>ID Tin Đăng</th>
                  <th>Tiêu Đề</th>
                  <th>Tóm Tắt</th>
                  <th>Thể Loại</th>
                  <th>Trạng Thái</th>
                  <th>Ngày Đăng</th>
                </tr>
              </thead>
              <tbody>
                @foreach($articleNew as $value)
                <tr>
                  <td>{{$value->id}}</td>
                  <td>{{$value->title}}</td>
                  <td>{{$value->recommend}}</td>
                  <td>{{$value->category->name}}</td>
                  <td>@if($value->status == '1' )
                    Hiện
                    @else
                    Ẩn
                    @endif
                  </td>
                  <td>{{date('d-m-Y', strtotime($value->created_at))}}</td>
                 <!--  <td>2011/04/25</td>
                  <td>$320,800</td> -->
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
             <div class="row justify-content-md-center" >          
              {!! $articleNew->links('paginate') !!}
        </div>
        </div>
        <!-- <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div> -->
      </div>
    </div>
@endsection