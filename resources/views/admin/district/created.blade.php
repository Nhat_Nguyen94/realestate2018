@extends('admin.layouts.master')
@section('style')
  <link href="/admin-css/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="/admin-css/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="/admin-css/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="/admin-css/css/sb-admin.css" rel="stylesheet">
@endsection
@section('content')
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="{{route('admin.index')}}">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Tables</li>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i>Thêm, Cập Nhật Quận Huyện</div>
        <div class="card-body">
    <form action="@if(isset($name))
    {{route('admin.district.update',['id'=>$name->id])}}
    @else
    {{route('admin.district.store')}}
    @endif" method="POST">
      <input type="hidden" name="_token" value="{{ csrf_token()}}">
     
   
     @if($errors->has('district'))
      <div class="alert alert-danger" role="alert">
         {{$errors->first('district')}}
      </div>
      @endif
  
    <div class="form-group">
    <label for="exampleInputEmail1">Quận Huyện</label>
    <input type="text" name="district" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nhập Tỉnh Thành" value="{{isset($name->name) ? $name->name : '' }}">
    </div>
    <div class="form-group">
    <label for="exampleInputEmail1">Chọn Tỉnh Thành</label>
    <select class="form-control" name="select_city">
       @foreach($city as $val)
      <option value="{{$val->id}}" {{isset($name) ? ($val->id == $name->city_id ? 'selected' : '') : '' }}>
        {{$val->name}}
      </option>
      @endforeach
    </select>
    </div>
   
    <button type="submit" class="btn btn-primary">Submit</button>
    </form>
        </div>
        <!-- <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div> -->
      </div>
    </div>
@endsection
@section('script')
    <!-- Bootstrap core JavaScript-->
    <script src="/admin-css/vendor/jquery/jquery.min.js"></script>
    <script src="/admin-css/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="/admin-css/vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="/admin-css/vendor/datatables/jquery.dataTables.js"></script>
    <script src="/admin-css/vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="/admin-css/js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="/admin-css/js/sb-admin-datatables.min.js"></script>
@endsection