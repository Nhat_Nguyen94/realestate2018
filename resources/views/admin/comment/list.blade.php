@extends('admin.layouts.master')

@section('content')
 <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Góp ý</a>
        </li>
      
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i>Góp ý</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <div class="col-md-12 search_add">
               <div class="row" >
                <div class="col-md-10 group_search">
                  <!-- <form class="form-inline" method="GET" action="" >
                    <input class="form-control col-md-4" name="search" value="{{isset($_GET['search'])?$_GET['search']:''}}" type="search" placeholder="Tìm" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0 btn_search" type="submit">Tìm Kiếm</button>
                  </form> -->
                </div>
              <!--   <div class="col-md-2 group_add">
                 <a class="btn btn-outline-success " href="" role="button">Thêm mới</a>
               </div>    -->                    
             </div>
           </div>
              
              <thead>
              	<tr>
              		<th>STT</th>
                  <th>Tiêu Đề</th>
              		<th>Nội Dung</th>
              		<th>User ID</th>
              		<th class="btn_delete">Tools</th>
              <!-- 		<th class="btn_update">Tools</th> -->
              	</tr>
              </thead>
              @if(isset($comment->first()->id))
              @foreach($comment as $key=>$value)
              <tbody>
              	<tr>
                  <td>{{$key + 1}}</td>
              		<td>{{$value->title}}</td>
              		<td>{!! $value->content !!}</td>
              		<td>{{$value->user->id}}</td>
              		<td class="btn_delete"><a class="btn btn-outline-danger" href="{{route('admin.comment.destroy',['id'=>$value->id])}}" >Xóa</a></td>
              	</tr>
              </tbody>
              @endforeach
              @else
              <tr>
              <td colspan="5"> 
              <div class="alert alert-info" role="alert">
              Không Có Góp ý 
              </div>
              </td>
              @endif
            </table>
          </div>
        <div class="row justify-content-md-center" >          
            
        </div>
        </div>
        <!-- <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div> -->
      </div>
    </div>
@endsection