<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Real Estate</title>

<!-- Bootstrap -->
<!-- <link href="https://fonts.googleapis.com/css?family=Libre+Franklin:100,200,300,400,500,700" rel="stylesheet"> -->

<link href="/realestate/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="/realestate/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<link href="/realestate/lib/animate.css" rel="stylesheet">
<link href="/realestate/lib/selectric/selectric.css" rel="stylesheet">
<link href="/realestate/lib/swiper/css/swiper.min.css" rel="stylesheet">
<link href="/realestate/lib/aos/aos.css" rel="stylesheet">
<link href="/realestate/lib/Magnific-Popup/magnific-popup.css" rel="stylesheet">
<link href="/realestate/css/style.css" rel="stylesheet">

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="/realestate/lib/jquery-3.2.1.min.js"></script>
<script src="/realestate/lib/popper.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/realestate/lib/bootstrap/js/bootstrap.min.js"></script>
<script src="/realestate/lib/selectric/jquery.selectric.js"></script>
<script src="/realestate/lib/swiper/js/swiper.min.js"></script>
<script src="/realestate/lib/aos/aos.js"></script>
<script src="/realestate/lib/Magnific-Popup/jquery.magnific-popup.min.js"></script>
<script src="/realestate/lib/sticky-sidebar/ResizeSensor.min.js"></script>
<script src="/realestate/lib/sticky-sidebar/theia-sticky-sidebar.min.js"></script>
<script src="/realestate/lib/lib.js"></script>
@yield('style')

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div id="main">
@include('realestate.layouts.include.nav')
@include('realestate.layouts.include.homesearch')
@yield('content')
@include('realestate.layouts.include.footer')
</div>
<script>
$( document ).ready(function() {
if($('.city_select').val() != 'all')
{
	showDistrict($('.city_select').val(),'{{isset($_GET["select_district"])?$_GET["select_district"]:0}}')
}
});
  $('.city_select').change(function(){
    console.log(this)
      showDistrict(this.value)
  }); 
     function showDistrict(valueCity,selected=0) {
    $.ajax({
    url: "{{route('realestate.searchdistrict')}}",
    type: 'POST',
    data:{id:valueCity, _token: '{{csrf_token()}}'},
    success:function(data){
      console.log(data);
      var json = $.parseJSON(data);
      // console.log(json);
      $('.districtsselect').html('');
      allContent = '<option value="all">Chọn Quận</option>';
      $.each(json, function(index, val) {
        if(selected != 0 && selected == val.id)
        {
        content = '<option value="'+val.id+'" selected>'+val.name+'</option>';
        }
        else
        {
        content = '<option value="'+val.id+'">'+val.name+'</option>';
        }
        allContent = allContent + content;
      });
      $('.districtsselect').html(allContent);
    }
  })
 }

</script>
@yield('script')

</body>
</html>