<div class="home-search">
  <div class="main search-form v2">
    <div class="container">
      <div class="row justify-content-md-center">
        <div class="col-md-12 col-lg-10">
          <div class="heading">
            <h2>Tìm ngay nhà mới của bạn !</h2>
            <h3>Chúng tôi sẽ giúp bạn tìm những nơi tốt nhất ở bất kỳ thành phố nào trên khắp Việt Nam.</h3>
          </div>
          <form action="{{route('realestate.search')}}">
            <div class="row justify-content-md-center" style="margin-bottom: 10px;">
              <div class="col-md-6">
                 <label for="">Nhập Từ Khóa</label>
                <div class="input-group input-group-lg">
                  <input type="text" class="form-control" name="txtName" placeholder="Nhập địa điểm..." value="{{isset($_GET['txtName'])?$_GET['txtName']:''}}">
                </div>     
              </div>
                <div class="col-md-6">
                <div class="row">         
                  <div class="col-md-12">
                     <?php $category =  DB::table('categories')->select()->where('status',1)->get(); ?>
                    <label for="">Chọn Loại Bất Động Sản</label>
                    <select id="inputState1" style="height: 48px" class="form-control" name="category">
                      <option value="">Chọn loại bất động sản</option>
                       @foreach($category as $val)
                      <option value="{{$val->id}}" {{isset($_GET['category']) && $_GET['category'] == $val->id?'selected':''}}>{{$val->name}}</option>
                      @endforeach
                    </select>          
                  </div>
                </div>
              </div>
            </div>
            <div class="row ">
              <div class="col-md-6">
                <div class="row">         
                  <div class="col-md-6">
                    <label for="">Giá Thấp Nhất</label>
                    <input type="number" name="min_price" class="form-control input-sm" placeholder="Đơn Giá VND">          
                  </div>
                  <div class="col-md-6">
                    <label for="">Giá Cao Nhất</label>
                    <input type="number" pattern="[0-9]*" name="max_price" class="form-control input-sm" placeholder="Đơn Giá VND">
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="inputState">Thành Phố</label>
                    <?php $city = DB::table('cities')->select()->get(); ?>
                    <select id="inputState2" class="form-control city_select" name="select_city">
                      <option value="all">Chọn Thành Phố</option>
                      @foreach($city as $value)                     
                      <option value="{{$value->id}}" {{isset($_GET['select_city']) && $_GET['select_city'] == $value->id?'selected':''}} >{{$value->name}}</option>
                      @endforeach                
                    </select>
                  </div>
                  <div class="form-group col-md-6">
                    <label for="inputState">Quận Huyện</label>
                    <select id="inputState3" class="form-control districtsselect" name="select_district">
                      <option value="all">Chọn Quận</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
            
            <span class="input-group-append">
               <!--      <button class="btn btn-white btn-lg" type="button"><i class="fa fa-map-marker" aria-hidden="true"></i></button> -->
                   <!--  <button class="btn btn-primary btn-lg" type="button">Search!</button> -->
                  </span>
              <button class="btn btn-primary btn-lg" type="submit" style="width: 100%">Tìm kiếm</button>     
          </form>
          
        </div>
      </div>
    </div>
  </div>
</div>