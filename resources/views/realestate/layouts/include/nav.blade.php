
<nav class="navbar navbar-expand-lg navbar-dark navbar-over absolute-top" id="menu">
  <div class="container">
    <a class="navbar-brand" href="{{route('realestate.index')}}"><span class="icon-uilove icon-uilove-realestate"></span></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu-content" aria-controls="menu-content" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="menu-content">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item">
          <a class="nav-link " href="{{route('realestate.index')}}" role="button" >
           Trang Chủ
          </a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Thể Loại
          </a>
          <?php $category =  DB::table('categories')->select()->where('status',1)->get(); ?>
          <div class="dropdown-menu">
            @foreach($category as $val)
            <a href="{{route('realestate.sortcategory',['id'=>$val->id])}}" class="dropdown-item" value="{{$val->id}}">{{$val->name}}</a>
            @endforeach

          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link " href="/contactus" role="button" >
           Liên Hệ
          </a>
        </li>
      </ul>
      @if(Auth::check())
      <ul class="navbar-nav ml-auto">
         <li class="nav-item dropdown user-account">
          <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class="user-image" style="background-image:url({{isset(Auth::user()->image) ? Auth::user()->image : '/realestate/img/demo/Screenshot_2.png'}});"></span>{{Auth::user()->name}}
          </a>
          <div class="dropdown-menu">
            <a href="{{route('user.index')}}" class="dropdown-item">Thông tin cá nhân</a>
            @if(Auth::user()->level == 2 )
            <a href="{{route('admin.index')}}" class="dropdown-item">Trang Quản Trị Viên</a>
            @endif
            <a href="{{route('user.passwordindex')}}" class="dropdown-item">Thay đổi mật khẩu</a>
            <a href="{{route('user.getarticle')}}" class="dropdown-item">Đăng Tin Bất Động Sản</a>
            <a href="{{route('user.commentindex')}}" class="dropdown-item">Góp ý</a>
            <a href="{{route('authen.logout')}}" class="dropdown-item" >Đăng Xuất</a>
          </div>
        </li>
      </ul>
     @else
      <ul class="navbar-nav ml-auto">
      <a href="{{route('authen.login')}}#form_login" class="btn btn-success" style="border-radius: 6px;margin-right: 10px;">Đăng Nhập</a>
      <a href="{{route('authen.register')}}#form_register" class="btn btn-success" style="border-radius: 6px;">Đăng Ký</a>
      </ul>
    @endif
    </div>
  </div>
</nav>
