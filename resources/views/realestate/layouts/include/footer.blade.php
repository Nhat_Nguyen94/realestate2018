<style>
	.container-1000 .col-sm-4{
		text-align: center;
		margin: auto;
	}
</style>
<button class="btn btn-primary btn-circle" id="to-top"><i class="fa fa-angle-up"></i></button>
<footer id="footer" class="bg-light footer-light">
	<div class="container container-1000">
		<div class="row">
			<div class="col-lg-3">
				<p><span class="icon-uilove icon-uilove-realestate"></span></p>
				<address class="mb-3">
					<strong>STU , Inc.</strong><br>
					83 Cao Lỗ, Quận 8<br>
					Thành Phố Hồ Chí Minh<br>
					<abbr title="Phone">P:</abbr>19001560
				</address>
				<div class="footer-social mb-4"><a href="#" class="ml-2 mr-2"><span class="fa fa-twitter"></span></a> <a href="#" class="ml-2 mr-2"><span class="fa fa-facebook"></span></a> <a href="#" class="ml-2 mr-2"><span class="fa fa-instagram"></span></a></div>
			</div>
			<div class="col-lg-2 col-sm-4">
				<div class="footer-links">
					<ul class="list-unstyled">
						<li class="list-title"><a href="/contactus">Liên Hệ</a></li>
						<!-- <li><a href="#">Company</a></li>
						<li><a href="#">Community</a></li>
						<li><a href="#">Carrers</a></li>
						<li><a href="#">Press</a></li>
						<li><a href="#">Contact</a></li> -->
					</ul>
				</div>
			</div>
			<div class="col-lg-4 col-sm-4">
				<div class="footer-links">
					<ul class="list-unstyled">
						<li class="list-title"><a href="#">Chính Sách Bảo Mật</a></li>
					<!-- 	<li><a href="#">For Rent</a></li>
						<li><a href="#">For Sale</a></li>
						<li><a href="#">Commercial</a></li>
						<li><a href="#">Agents</a></li>
						<li><a href="#">Property Guides</a></li>
						<li><a href="#">Jobs</a></li> -->
					</ul>
				</div>
			</div>
			<div class="col-lg-2 col-sm-4">
				<div class="footer-links">
					<ul class="list-unstyled">
						<li class="list-title"><a href="#">Thắc Mắc</a></li>
						<!-- <li><a href="#">Payments</a></li>
						<li><a href="#">Shipping</a></li>
						<li><a href="#">Cancellation</a></li>
						<li><a href="#">FAQ</a></li>
						<li><a href="#">Report</a></li> -->
					</ul>
				</div>
			</div>
			<!-- <div class="col-lg-3">
				<div class="text-lg-right ml-lg-2">
					<form>
						<div class="list-title">Subscribe</div>
						<div class="input-group input-group-lg">
							<input type="text" name="email" class="form-control form-control-lg subscribe-input" placeholder="Email">
							<div class="input-group-append ml-0">
								<button class="btn subscribe-button" type="button"><i class="fa fa-envelope"></i></button>
							</div>
						</div>
					</form>
					<div class="footer-payments"><span class="fa fa-cc-visa"></span> <span class="fa fa-cc-mastercard"></span> <span class="fa fa-cc-amex"></span> </div>
				</div>
			</div> -->

		</div>
		<div class="footer-credits d-lg-flex justify-content-lg-between align-items-center">
			<div>Powered by <strong>Minh Nhật</strong></div>

			<div>© 2018 Minh Nhật All Rights Reserved</div>
		</div>
	</div>
</footer>