@extends('realestate.layouts.master')

@section('content')
<div class="container">
	<div class="row row-no-padding">
		<div class="contact_page" style="border: 1px solid #ccc;padding: 10px;margin: 40px 120px 0px 120px;">
			<strong class="title">Liên hệ với chúng tôi</strong>
			<div>
				<p>Bộ phận chăm sóc khách hàng của Nhật Website xin chào bạn. Cám ơn bạn đã dành thời gian liên lạc với chúng tôi. Chúng tôi trân trọng các ý kiến đóng góp của bạn. Chúng tôi sẽ liên lạc với bạn trong thời gian sớm nhất. Dưới đây là các cách bạn có thể liên lạc với chúng tôi.</p>

				<p><b>Liên lạc với Phòng Dịch Vụ Khách Hàng</b><br>
					Trong trường hợp bạn gặp khó khăn khi sử dụng trang web của chúng tôi, có thể những thắc mắc của các bạn đã có sẵn phần trả lời trong mục&nbsp;<b>Trợ giúp</b>&nbsp;của chúng tôi.<br>
					Trong trường hợp bạn có những yêu cầu, phản hồi hoặc đề nghị, bạn vui lòng gửi về email <a href="mailto:nhat.nguyenminh94@gmail.com">nhat.nguyenminh94@gmail.com</a></p>

					<p><b>Văn phòng Nhật Website</b>

						<br>Cao lỗ, STU, Quận 8, Tp.HCM

						<br>Điện thoại: 1900 xxx. Fax: (84-8) 391xxx. Email:&nbsp;<a href="mailto:nhat.nguyenminh94@gmail.com">nhat.nguyenminh94@gmail.com</a>, <a href="mailto:nhat.nguyenminh94@gmail.com">nhat.nguyenminh94@gmail.com</a></p>
					</div>
				</div>
			</div>
		</div>
@endsection