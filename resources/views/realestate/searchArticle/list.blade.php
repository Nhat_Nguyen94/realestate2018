@extends('realestate.layouts.master')
@section('content')
<style>
  #map{

        height: 800px;  /* The height is 400 pixels */
        width: 100%;  /* The width is the width of the web page */
       
  }
   .pagination{
    margin :0 auto !important;
  }
</style>
 <div class="container">
    <div class="row justify-content-md-center">
          <div class="col col-lg-12 col-xl-10">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Trang chủ</a></li>
            <li class="breadcrumb-item">
           	<a href="#">
             
            </a>
        	</li>
            </ol>
            </nav>
        <div class="page-header">
        <h1>Tất cả Bất động sản bạn tìm  </h1>
        </div>
      </div>
    </div>
  </div>
	<div id="content">
    <div class="container">
    <div class="row justify-content-md-center">
          <div class="col col-lg-12 col-xl-10">
        <div class="row">
          <div class="col-md-7">
  <!--           <div class="sorting">
              <div class="row justify-content-between">
              <div class="col-sm-5 col-md-5 col-lg-4 col-xl-3">
              </div>
              <div class="col-sm-6 col-md-5 col-lg-4 col-xl-3">
              <div class="btn-group float-right" role="group"> <a href="property_grid.html" class="btn btn-light"><i class="fa fa-th"></i></a> <a href="property_listing.html" class="btn btn-light active"><i class="fa fa-bars"></i></a> </div>
              </div>
              </div>                
            </div> -->
            <div class="clearfix"></div>
            <div class="item-listing list">
            @if(!isset($searchArticle->first()->id))
				<div class="alert alert-danger" role="alert">
					Mục này không có Bất Động Sản.
				</div>
			@else
              @foreach($searchArticle as $val)
              <div class="item">
                <div class="row">
                  <div class="col-lg-5">
                    <div class="item-image"> <a href="{{route('realestate.detail',['id'=>$val->id])}}"><img src="/{{$val->images[0]->src}}" class="img-fluid" alt="" style="height: 200px;">
                    <!--   <div class="item-badges">
                      <div class="item-badge-left">Sponsored</div>
                      <div class="item-badge-right">For Sale</div>
                      </div> -->
                      <div class="item-meta">
                      <div class="item-price">${{ number_format($val->price) }} VNĐ
                      <small></small>
                      </div>
                      </div>
                      </a>
                      @if($val->pin == 1)
                      <a href="javascript:void(0)" class="save-item"  data-toggle="popover" data-placement="top" data-content="Tin ưu tiên"><i class="fa fa-star"></i></a> 
                       @endif 
                    </div>
                  </div>
                  <div class="col-lg-7">
                  <div class="item-info">
                    <h3 class="item-title"><a href="{{route('realestate.detail',['id'=>$val->id])}}">{{$val->title}}</a></h3>
                     <div class="row">
                      <div class="col-md-6">
                           <div class="item-location"><i class="fa fa-map-marker"></i> Địa Chỉ: {{$val->address}}</div>
                      </div>
                      <div class="col-md-6">
                           <div class="item-location"><i class="fa fa-check-circle"></i> Giấy Tờ: {{$val->document}}</div>
                      </div>
                    </div>
                     <div class="row">
                      <div class="col-md-6">
                         <div class="item-location"><i class="fa fa-home"></i> Diện Tích: {{$val->area}} m2</div>
                      </div>
                      <div class="col-md-6">
                           <div class="item-location"><i class="fa fa-arrows"></i> Hướng: {{$val->direction}}</div>
                      </div>
                    </div>
                 
                   
                    <div class="item-details-i">Số Phòng Ngủ: <span class="bedrooms" data-toggle="tooltip" title="{{$val->bedroom}} Phòng Ngù">{{$val->bedroom}} <i class="fa fa-bed"></i></span> Số Phòng Tắm:<span class="bathrooms" data-toggle="tooltip" title="{{$val->bathroom}}  Phòng Tắm"> {{$val->bathroom}} <i class="fa fa-bath"></i></span>

                     </div>
                    <div class="item-details">
                      <ul>
                        <!-- <li>Sq Ft <span>730-2600</span></li> -->
                        <li>Loại Bất Động Sản <span>{{$val->category->name}}</span></li>
                        <li>Quận Huyện<span>{{$val->district->name}}</span></li>
                      </ul>
                    </div>
                 </div>
                     <div class="row">
                    <div class="col-md-6">
                                        <div class="added-on">Đăng ngày {{date('d-m-Y', strtotime($val->created_at))}} </div>

                    </div>
                    <div class="col-md-6">
                                         <a href="#" class="added-by">bởi {{$val->user->name}}</a>

                    </div>
                    </div>
                  </div>
                </div>
              </div>
            @endforeach
             @endif
            </div>
        
          </div>
           <div class="col-md-5">
                 <div id="map"></div>
           @foreach($searchArticleAll as $value)
           <input type="hidden" name="lat[]" class="lat" value="{{$value->lat}}">
            <input type="hidden" name="long[]" class="long" value="{{$value->long}}">
            @endforeach
           </div>
        </div>
        </div>
        </div>
        </div>
        </div>
               <div class="col-md-12">
               <div class="row">
              {!! $searchArticle->appends($_GET)->links('paginate') !!}
              </div>
        </div>
@endsection
@section('script')
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC7I839c4Padwja-_7ZIvtY_RXAnGt3xnM&callback=initMap">
    </script>
<script>
  $(document).ready(function() {
  $(function () {
  $('[data-toggle="popover"]').popover()
})
});


  function initMap() {
var lat;
var long;

lat= '10.7694307' ;
long='106.6854644';
var _location = new google.maps.LatLng( lat, long );console.log(_location); 
var _mapOptions = {center: _location,zoom: 9,scrollwheel: false};
map = new google.maps.Map(document.getElementById("map"), _mapOptions);
var bounds = new google.maps.LatLngBounds();
var lats = $("input[name='lat[]']").map(function(){return $(this).val();}).get();
console.log(lats);
var longs = $("input[name='long[]']").map(function(){return $(this).val();}).get();
console.log(longs);

for (var i = 0; i < lats.length; i++) {
  var lat = parseFloat(lats[i]);
  var long = parseFloat(longs[i]);
  var pos = new google.maps.LatLng(lat, long);
  var marker = new google.maps.Marker({
          position: {lat: lat, lng: long},
          map: map
        });
  bounds.extend(pos);
}

map.fitBounds(bounds);
map.setZoom(8);
      // map = new google.maps.Map(document.getElementById('map'), {
      //    center: {lat: null, lng: null},
      //   zoom: 9,
      // });

      // var lats = $("input[name='lat[]']").map(function(){return $(this).val();}).get();
      // console.log(lats);
      // var longs = $("input[name='long[]']").map(function(){return $(this).val();}).get();
      // console.log(longs);

      // for(var i = 0; i < lats.length; i++) {
      //   var lat = parseFloat(lats[i]);
      //   var long = parseFloat(longs[i]);

      //   var marker = new google.maps.Marker({
      //     position: {lat: lat, lng: long},
      //     map: map
      //   });

      // }

      // $('input[type=hidden]').map(function(){
      //  var stops = [];
      //       stops =  this.value;
      //   console.log(stops);
      //   for(var i=0; i<=stops.length; i++){
      //    // console.log(i);
      //   var mypos = parseFloat({lat: stops[i].lat, lng: stops[i].long});
      //  // console.log(mypos);
      //   var marker = new google.maps.Marker({
      //   position: mypos,
      //   map: map,
      //   });
      //   }
      //   }).get();

  // var latitude = parseFloat($('#lat').val());
  // var longitude = parseFloat($('#long').val());
  // var uluru = {lat: latitude, lng:longitude};
  // var map = new google.maps.Map(
  //     document.getElementById('map'), {zoom: 14, center: uluru});
  // var marker = new google.maps.Marker({position: uluru, map: map});
  //     var infowindow = new google.maps.InfoWindow({
  //       content: $('#address_text').text(),
  
  //   });
  //      infowindow.open(map,marker);
}
  </script>
@endsection