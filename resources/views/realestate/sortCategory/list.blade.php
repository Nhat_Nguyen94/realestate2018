@extends('realestate.layouts.master')
@section('content')
<style>
    .pagination{
    margin :0 auto !important;
  }
</style>
 <div class="container">
    <div class="row justify-content-md-center">
      @if(isset($sortCate->first()->id))
          <div class="col col-lg-12 col-xl-10">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Trang chủ</a></li>
            <li class="breadcrumb-item">
           	<a href="#">  {{$sortCate[0]->category->name}}
            </a>
        	</li>
            </ol>
            </nav>
        <div class="page-header">
        <h1>Tất cả Bất động sản của   {{$sortCate[0]->category->name}}</h1>
        </div>
      </div>
      @endif
    </div>
  </div>
	<div id="content">
    <div class="container">
    <div class="row justify-content-md-center">
          <div class="col col-lg-12 col-xl-10">
        <div class="row has-sidebar">
       <!--    <div class="col-md-4 col-lg-3">
            <button id="toggle-filters" class="btn btn-primary btn-circle mobile-filter"><i class="fa fa-filter"></i></button>
            <div id="sidebar" class="sidebar-left">
            <button class="close-panel btn btn-white"><i class="fa fa-long-arrow-left"></i></button>
              <div class="sidebar_inner">
              <div id="filters">
                <div class="card">
                  <div class="card-header">
                    <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#p_budget" aria-expanded="true" aria-controls="p_type"> Budget <i class="fa fa-caret-down float-right"></i> </a> </h4>
                  </div>
                  <div id="p_budget" class="panel-collapse collapse" role="tabpanel">
                    <div class="card-body">
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <input type="text" class="form-control input-sm" placeholder="Min">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <input type="text" class="form-control input-sm" placeholder="Max">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header">
                    <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#p_type" aria-expanded="true" aria-controls="p_type"> Property Type <i class="fa fa-caret-down float-right"></i> </a> </h4>
                  </div>
                  <div id="p_type" class="panel-collapse collapse show" role="tabpanel">
                    <div class="card-body">
                        <div class="checkbox ">
                            <input type="checkbox" value="1" id="house">
                            <label for="house">House</label>
                        </div>
                        <div class="checkbox ">
                            <input type="checkbox" value="1" id="flat">
                            <label for="flat">Flat</label>
                        </div>
                        <div class="checkbox ">
                            <input type="checkbox" value="1" id="appartment">
                            <label for="appartment">Appartment</label>
                        </div>
                        <div class="checkbox ">
                            <input type="checkbox" value="1" id="farms">
                            <label for="farms">Farms/Lands</label>
                        </div>
                        <div class="checkbox ">
                            <input type="checkbox" value="1" id="room">
                            <label for="room">Room</label>
                        </div>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header">
                    <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#p_features" aria-expanded="true" aria-controls="p_features"> Features <i class="fa fa-caret-down float-right"></i> </a> </h4>
                  </div>
                  <div id="p_features" class="panel-collapse collapse show" role="tabpanel">
                    <div class="card-body">
                      <div class="checkbox">
                        <input type="checkbox" value="" id="garden">
                        <label for="garden"> Garden</label>
                      </div>
                      <div class="checkbox">
                        <input type="checkbox" value="" id="parking">
                        <label for="parking"> Parking</label>
                      </div>
                      <div class="checkbox">
                        <input type="checkbox" value="" id="fireplace">
                        <label for="fireplace"> Fireplace</label>
                      </div>
                      <div class="checkbox">
                        <input type="checkbox" value="" id="restaurant">
                        <label for="restaurant"> Restaurant</label>
                      </div>
                      <div class="checkbox">
                        <input type="checkbox" value="" id="school">
                        <label for="school"> School</label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              </div>
            </div>
          </div> -->
          <div class="col-md-8 col-lg-9">
            <div class="sorting">
              <div class="row justify-content-between">
              <div class="col-sm-5 col-md-5 col-lg-4 col-xl-3">
            <!--   <div class="form-group">
                  <select class="form-control ui-select">
                    <option selected="selected">Most recent</option>
                    <option>Highest price</option>
                    <option>Lowest price</option>
                    <option>Most reduced</option>
                    <option>Most popular</option>
                  </select>
                </div> -->
              </div>
<!--               <div class="col-sm-6 col-md-5 col-lg-4 col-xl-3">
              <div class="btn-group float-right" role="group"> <a href="property_grid.html" class="btn btn-light"><i class="fa fa-th"></i></a> <a href="property_listing.html" class="btn btn-light active"><i class="fa fa-bars"></i></a> </div>
              </div> -->
              </div>                
            </div>
            <div class="clearfix"></div>
            <div class="item-listing list">
            @if(!isset($sortCate->first()->id))	
				<div class="alert alert-danger" role="alert">
					Mục này không có Bất Động Sản.
				</div>
			@else
              @foreach($sortCate as $val)
              <div class="item">
                <div class="row">
                  <div class="col-lg-5">
                    <div class="item-image"> <a href="{{route('realestate.detail',['id'=>$val->id])}}"><img src="/{{$val->images[0]->src}}" class="img-fluid" alt="" style="height: 200px;">
                  <!--     <div class="item-badges">
                      <div class="item-badge-left">Sponsored</div>
                      <div class="item-badge-right">For Sale</div>
                      </div> -->
                      <div class="item-meta">
                      <div class="item-price">${{ number_format($val->price) }}
                      <small></small>
                      </div>
                      </div>
                      </a>
                     @if($val->pin == 1)
                      <a href="javascript:void(0)" class="save-item"  data-toggle="popover" data-placement="top" data-content="Tin ưu tiên"><i class="fa fa-star"></i></a> 
                       @endif </div>
                  </div>
                  <div class="col-lg-7">
                  <div class="item-info">
                    <h3 class="item-title"><a href="{{route('realestate.detail',['id'=>$val->id])}}">{{$val->title}}</a></h3>
                    <div class="row">
                      <div class="col-md-6">
                           <div class="item-location"><i class="fa fa-map-marker"></i> {{$val->address}}</div>
                      </div>
                      <div class="col-md-6">
                           <div class="item-location"><i class="fa fa-map-marker"></i>Giấy Tờ {{$val->document}}</div>
                      </div>
                    </div>
                     <div class="row">
                      <div class="col-md-6">
                         <div class="item-location"><i class="fa fa-map-marker"></i>Diện Tích {{$val->area}} m2</div>
                      </div>
                      <div class="col-md-6">
                           <div class="item-location"><i class="fa fa-map-marker"></i>Hướng {{$val->direction}}</div>
                      </div>
                    </div>
                 
                 
                   
                 
                    <div class="item-details-i"> <span class="bedrooms" data-toggle="tooltip" title="3 Bedrooms">3 <i class="fa fa-bed"></i></span> <span class="bathrooms" data-toggle="tooltip" title="2 Bathrooms">Quận Huyện : {{$val->district->name}} <i class="fa fa-bath"></i></span>

                     </div>
                    <div class="item-details">
                      <ul>
                     <!--    <li>Sq Ft <span>730-2600</span></li> -->
                        <li>Loại <span>{{$val->category->name}}</span></li>
                      </ul>
                    </div>
                 </div>
                    <div class="row">
                    <div class="col-md-6">
                                        <div class="added-on">Ngày Đăng{{date('d-m-Y', strtotime($val->created_at))}} </div>

                    </div>
                    <div class="col-md-6">
                                         <a href="#" class="added-by">{{$val->user->name}}</a>

                    </div>
                    </div>
                  </div>
                </div>
              </div>
            @endforeach
             @endif
            </div>
        
          </div>
           
        </div>
        </div>
        </div>
        </div>
        </div>
         <div class="col-md-12">
               <div class="row">
              {!! $sortCate->links('paginate') !!}
              </div>
        </div>
@endsection
