@extends('realestate.layouts.master')
@section('style')

<!-- style detail -->
<!-- Bootstrap -->
<link href="https://fonts.googleapis.com/css?family=Libre+Franklin:100,200,300,400,500,700" rel="stylesheet">

<link href="/realestate/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="/realestate/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<link href="/realestate/lib/animate.css" rel="stylesheet">
<link href="/realestate/lib/selectric/selectric.css" rel="stylesheet">
<link href="/realestate/lib/swiper/css/swiper.min.css" rel="stylesheet">
<link href="/realestate/lib/aos/aos.css" rel="stylesheet">
<link rel="stylesheet" href="/realestate/lib/photoswipe/photoswipe.css"> 
<link rel="stylesheet" href="/realestate/lib/photoswipe/default-skin/default-skin.css"> 
<link href="/realestate/css/style.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="/slick/slick.css"/>
<link rel="stylesheet" type="text/css" href="/slick/slick-theme.css"/>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="/realestate/lib/jquery-3.2.1.min.js"></script>
<script src="/realestate/lib/popper.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/realestate/lib/bootstrap/js/bootstrap.min.js"></script>
<script src="/realestate/lib/selectric/jquery.selectric.js"></script>
<script src="/realestate/lib/swiper/js/swiper.min.js"></script>
<script src="/realestate/lib/aos/aos.js"></script>
<script src="/realestate/lib/sticky-sidebar/ResizeSensor.min.js"></script>
<script src="/realestate/lib/sticky-sidebar/theia-sticky-sidebar.min.js"></script>
<script src="/realestate/lib/lib.js"></script>
<style>
  .swiper-button-next, .swiper-button-prev {
    height: 10%;
  }
  figure {
    margin: 0 auto !important;
  }
    #map {
        height: 400px;  /* The height is 400 pixels */
        width: 100%;  /* The width is the width of the web page */
       }
       #carouselExampleControls a span{
        background-color: #ff5f5f
       }
</style>
@endsection
@section('content')
<div class="container">
  <div class="row justify-content-md-center">
          <div class="col col-md-12 col-lg-12 col-xl-10">
      <div class="page-header bordered mb0">
        <div class="row">
          <div class="col-md-8"> <a href="#" class="btn-return" title="Back"><i class="fa fa-angle-left"></i></a>
            <h1>{{$article->title}} <span class="label label-bordered">For sale</span> <small><i class="fa fa-map-marker"></i>{{$article->address}}</small></h1>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="content" class="item-single">
  <div class="container">
    <div class="row justify-content-md-center">
          <div class="col col-md-12 col-lg-12 col-xl-10">
        <div class="row">
        <div class="row row justify-content-md-center col-md-7">
          <div class="">
            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
              <div class="carousel-inner">
                @foreach($article->images as $value)
                <div class="carousel-item ">
                  <img class="d-block w-100" src="/{{$value->src}}" alt="First slide" width="200" height="200">

                </div>
                @endforeach
              </div>
              <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev" >
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>   
              <div>
                <ul class="item-features">
                  <li>Giá<span>{{number_format($article->price)}}VNĐ</span></li>
                  <li>Giấy tờ<span>{{$article->document}}</span></li>
                  <li>Diện Tích<span>{{$article->area}} m2</span></li>
                  <li>Địa chỉ <span id="address_text">{{$article->address}}</span></li>
                </ul>
                <div class="item-description">
                  <h3 class="headline">Nội dung tin đăng</h3>
                  {!! $article->content !!}
                </div>
              
              <h3 class="headline">Các tiện ích xung quanh</h3>
              <ul class="checked_list feature-list">
                <li>Bệnh Viện</li>
                <li>Phòng Gym</li>
                <li>Internet</li>
                <li>Hồ Bơi</li>
                <li>Khu Thương Mại</li>
              </ul>
              <h3>Địa điểm bất động sản bằng Google Map</h3>
              <!--The div element for the map -->
              <div id="map"></div>
              <input type="hidden" name="lat" id="lat" value="{{$article->lat}}">
              <input type="hidden" name="long" id="long" value="{{$article->long}}">
              </div>
            </div>
          </div>
          <div class="col-md-5 col-lg-4">
            <div id="sidebar" class="sidebar-right">
              <div class="sidebar_inner">
                <div class="card shadow">
                  <h5 class="subheadline mt-0  mb-0">Được Đăng Bởi </h5>
                  <div class="media">
                    <div class="media-left"> <a href="agent.html"> <img class="media-object rounded-circle" src="{{$article->user->image}}" width="64" height="64" alt=""> </a> </div>
                    <div class="media-body">
                      <h4 class="media-heading"><a href="agent.html">{{$article->user->name}}</a></h4>
                      <p><a href="tel:01502392905"><i class="fa fa-phone" aria-hidden="true"></i> Số Điện Thoại: {{$article->user->phone}}</a></p>
                      <p><a href="#" class="btn btn-sm btn-light" style="position: relative;
    left: -80px;">{{$article->user->email}}</a></p>
                      <p></p>
                      @if(Auth::check())
                      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
                      Gửi Tin Nhắn
                      </button>
                      @endif
                    </div>
                  </div>
              </div>
            </div>
          </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Nội Dung Liên Lạc</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="{{route('user.contact.addcontact')}}" method="get">
          <div class="form-group">
            <label for="exampleInputEmail1"></label>
            <textarea  class="form-control" name="content_contact" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nhập Nội Dung"></textarea>
          </div>
          <input type="hidden" name="user_from" value="{{isset(Auth::user()->id) ? Auth::user()->id : '' }}">
          <input type="hidden" name="user_to" value="{{$article->user->id}}">
          <input type="hidden" name="article_id" value="{{$article->id}}">
        <!--   <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
          </div> -->
         <!--  <div class="form-check">
            <input type="checkbox" class="form-check-input" id="exampleCheck1">
            <label class="form-check-label" for="exampleCheck1">Check me out</label>
          </div> -->
          <button type="submit" class="btn btn-primary">Gửi</button>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection('content')
@section('script')
     <script type="text/javascript" src="/slick/slick.min.js"></script>
     <script>
        $(document).ready(function(){
          $('.carousel-item:first').addClass('active');
          // $('.carousel').carousel({
          // interval: 2000
          // })
        });
     </script>
  <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC7I839c4Padwja-_7ZIvtY_RXAnGt3xnM&callback=initMap">
    </script>
    <script>  
  function initMap() {
  var latitude = parseFloat($('#lat').val());
  var longitude = parseFloat($('#long').val());
  var uluru = {lat: latitude, lng:longitude};
  var map = new google.maps.Map(
      document.getElementById('map'), {zoom: 14, center: uluru});
  var marker = new google.maps.Marker({position: uluru, map: map});
      var infowindow = new google.maps.InfoWindow({
        content: $('#address_text').text(),
  
    });
       infowindow.open(map,marker);
}
    </script>
@endsection