@extends('realestate.layouts.master')

@section('content')
@if(Auth::check())
<div id="content">
  <div class="container">
    <div class="row justify-content-md-center">
          <div class="col col-lg-12 col-xl-10">
        <div class="row has-sidebar">
          @include('realestate.users.menuleft')
          <div class="col-md-7 col-lg-8 col-xl-8">
            <div class="page-header bordered">
              <h1>Tài Khoản Của Bạn<!--  <small>Manage your public profile</small> --></h1>
            </div>
            <form action="{{route('user.update',['id'=>Auth::user()->id])}}" method="post" enctype="multipart/form-data">
              <input type="hidden" name="_token" value="{{ csrf_token()}}">
              @if(session('status') == 1)
                <div class="alert alert-success" role="alert">
                Cập Nhật Thông Tin Thành Công
                </div>
              @endif
              @if ($errors->any())
              <div class="alert alert-danger">
                <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
                </ul>
              </div>
              @endif
              <h3 class="subheadline">Thông tin cá nhân </h3>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Họ Và Tên</label>
                    <input type="text" name="name" class="form-control form-control-lg" placeholder="" value="{{Auth::user()->name}}">
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label>Email</label>
                <input type="text" name="email" class="form-control form-control-lg" disabled value="{{Auth::user()->email}}">
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Số điện thoại</label>
                    <input type="text" name="phone" class="form-control form-control-lg" placeholder="" value="{{Auth::user()->phone}}">
                  </div>
                </div>
              </div>
              <div class="col-md-12">
              <div class="row"> 
                  
                    <label>Hình Ảnh Đại diện</label>
                    <div class="col col-md-4">
                      <label for="file-input" class=" form-control-label">File input</label>
                      <input type="file" id="file-input" name="image_upload filesTest" class="form-control-file upload-image" onchange="UploadImage(this);">
                    </div>
                    <div class="col col-md-6">                     
                     <div class="media-left"> <a href="agent.html">
                      <img class="media-object rounded-circle" name="image" id="image_show" src="{{isset(Auth::user()->image) ? Auth::user()->image : '/realestate/img/demo/Screenshot_2.png'}}" width="64" height="64" alt=""> 
                      <input type="hidden" name="image" id="images_show_data" value="/realestate/img/demo/Screenshot_2.png"></a>
                    </div>
                    </div>
                  
              </div>
              </div>
              <hr>
              <div class="form-group action">
                <button type="submit" class="btn btn-lg btn-primary">Cập Nhật Thông Tin</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endif
@endsection
@section('script')
<script>
function UploadImage(e){
  
    var form_data = new FormData();
        form_data.append('file', e.files[0]);
        form_data.append('_token', '{{csrf_token()}}');
        $.ajax({
        url: "{{route('user.uploadimage',['type'=>'user'])}}", // Url to which the request is send
        data: form_data,
        type: 'POST',
        contentType: false,
        processData: false,
        success: function(res)   // A function to be called if request succeeds
        {
          dataSuccess = JSON.parse(res);
          // console.log(res);
          console.log(dataSuccess.status);
          if(dataSuccess.status == false){
            alert(dataSuccess.errors.file);
          }else{
          $('#image_show').attr('src','');
          $('#images_show_data').val('');
          console.log(dataSuccess.filename);
          $('#image_show').attr('src', '/'+dataSuccess.filename);
          $('#images_show_data').val('/'+dataSuccess.filename);
          }
         
        },error: function(error){
            alert(error);
            }
        });
        };

</script>          
@endsection