@extends('realestate.layouts.master')

@section('content')
<div id="content">
  <div class="container">
    <div class="row justify-content-md-center">
          <div class="col col-lg-12 col-xl-10">
        <div class="row has-sidebar">
          @include('realestate.users.menuleft')
          <div class="col-md-7 col-lg-8 col-xl-8">
            <div class="page-header bordered">
              <h1>Góp ý Của Bạn<!--  <small>Manage your public profile</small> --></h1>
            </div>
            <form action="{{route('user.createdcomment')}}" method="post" enctype="multipart/form-data">
              <input type="hidden" name="_token" value="{{ csrf_token()}}">
              @if(session('status') == 13)
                <div class="alert alert-success" role="alert">
                Góp ý Thành Công
                </div>
              @endif
              @if ($errors->any())
              <div class="alert alert-danger">
                <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
                </ul>
              </div>
              @endif
            <!--   <h3 class="subheadline">Thông tin cá nhân </h3> -->
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Tiêu đề</label>
                    <input type="text" name="title" class="form-control form-control-lg" placeholder="" value="">
                  </div>
                </div>
              </div>
              <div class="form-group">
              	<label for="inputAddress2">Nội Dung</label>
              	<textarea name="content" id="textarea-input" rows="3" placeholder="" class="form-control ckeditor" value=""></textarea>
              	<script>
              		CKEDITOR.replace( 'content' );
              	</script>
              </div>
              <hr>
              <div class="form-group action">
                <button type="submit" class="btn btn-lg btn-primary">Góp ý</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<script src="//cdn.ckeditor.com/4.9.1/standard/ckeditor.js"></script>
@endsection