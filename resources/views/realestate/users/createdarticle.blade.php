@extends('realestate.layouts.master')
@section('style')
  <!-- Custom fonts for this template-->
  <link href="/admin-css/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <!-- Custom styles for this template-->
  <script src="//cdn.ckeditor.com/4.9.1/standard/ckeditor.js"></script>
  <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('content')
 <style>
       /* Set the size of the div element that contains the map */
      #map {
        height: 400px;  /* The height is 400 pixels */
        width: 100%;  /* The width is the width of the web page */
       }
    </style>
@if(Auth::check())
<div id="content">
<div class="row">
@include('realestate.users.menuleft')
<div class="col-md-7">
	 <div class="container-fluid">
      <!-- Breadcrumbs-->
     
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i>@if(isset($name)) Cập Nhật Tin Đăng @else Thêm Tin Đăng @endif</div>
        <div class="card-body">
           @if(session('created') == 3)
                <div class="alert alert-success" role="alert">
                Đăng Tin Thông Tin Thành Công
                </div>
            @endif
          <form action="{{route('user.postarticle')}}" class="form_article" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token()}}">

          @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
              @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
          @endif
            <div class="form-row">
              <div class="form-group col-md-12">
                <label for="inputEmail4">Tiêu Đề</label>
                <input type="text" name="title" class="form-control" id="title" placeholder="Tiêu Đề" value="{{old('title',isset($name->title)?$name->title:'') }}">
              </div>
              <div class="form-group col-md-12">
                <label for="inputState">Thê Loại</label>
                <select id="inputState" class="form-control" name="select_category">
                  @foreach($category as $value)
                  <option value="{{$value->id}}" {{isset($name) ? ($value->id == $name->category_id ? 'selected' : '') : '' }}>{{$value->name}}</option>
                  @endforeach
                </select>
              </div>

            </div>
            <div class="form-row">

              <div class="form-group col-md-12">
                <label for="inputState">Thành phố</label>
                <select id="inputState" class="form-control city_select">
                  <option value="">Chọn Thành Phố</option>
                  @foreach($city as $val)
                  <option value="{{$val->id}}" {{isset($name) ? ($val->id == $name->district->city->id ? 'selected' : '') : '' }} >{{$val->name}}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group col-md-12">
                <label for="inputState">Quận Huyện</label>
                <select id="inputState" class="form-control districtsselect" name="select_district">
                  <option>Chọn Quận</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="inputAddress">Tóm tắt</label>
              <input type="text" name="recommend" class="form-control" id="inputAddress" placeholder="Tóm tắt" value="{{isset($name->recommend) ? $name->recommend : '' }}">
            </div>
            <div class="form-group">
              <div class="col-md-12 image_content">
                <div class="row">
                  <button type="button" class="btn btn-primary btn_addimg" value="0" >Thêm Hình</button>
                </div>
                <div class="Add_img">
                   
                 
                </div>
              </div>
            </div>
          

            @if($errors->has('file'))
              <div class="alert alert-danger" role="alert">
              {{$errors->first('file')}}
              </div>
            @endif
            <div class="form-group">
              <label for="inputAddress2">Nội dung</label>
              <textarea name="content123" id="textarea-input" rows="3" placeholder="content" class="form-control ckeditor" value=""> </textarea>
              <script>
                 CKEDITOR.replace( 'content123' );
              </script>
             

            </div>
             <h3>Chọn địa điểm bằng Google Map</h3>
            <!--The div element for the map -->
            <div id="map"></div>
            <input type="hidden" name="lat" id="lat" value="0">
            <input type="hidden" name="long" id="long" value="0">
            <div class="form-group">
             <div class="row">
              <div class="col">
                <label for="inputAddress2">Giấy Tờ</label>
                <input type="text" name="document" class="form-control" value="" placeholder="Giấy Tờ">
              </div>
              <div class="col">
                 <label for="inputAddress2">Địa Chỉ</label>
                <input type="text"  name="address" class="form-control" placeholder="Địa Chỉ" value="">
              </div>
              <div class="col">
                 <label for="inputAddress3">Số Phòng Ngủ</label>
                <input type="number"  name="bedroom" class="form-control" placeholder="Số phòng ngủ" value="">
              </div>
                 <div class="col">
                 <label for="inputAddress3">Số Phòng Tắm</label>
                <input type="number"  name="bathroom" class="form-control" placeholder="Phòng tắm" value="">
              </div>
            </div>
             <div class="row">
              <div class="col">
                <label for="inputAddress2">Hướng</label>
                <input type="text"  name="direction" class="form-control" placeholder="Hướng" value="{{isset($name->direction) ? $name->direction : '' }}">
              </div>
              <div class="col">
                 <label for="inputAddress2">Diện tích nhà</label>
                <input type="text" name="area" class="form-control" placeholder="Diện tích" value="{{isset($name->area) ? $name->area : '' }}">
              </div>
               <div class="col">
                 <label for="inputAddress2">Giá Tiền</label>
                <input type="text" name="price" class="form-control" placeholder="Giá tiên" value="{{isset($name->price) ? $name->price : '' }}">
              </div>
            </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="inputZip">Ngày bắt đầu</label>
                <input type="date" name="start_date"class="form-control" id="inputZip" value="{{isset($name->start_date) ? $name->start_date : '' }}">
              </div>
              <div class="form-group col-md-6">
                <label for="inputZip">Ngày Kết Thúc</label>
                <input type="date" name="end_date" class="form-control" id="inputZip"  value="{{isset($name->end_date) ? $name->end_date : '' }}">
              </div>
            </div>
            <button type="submit" class="btn btn-primary" >Đăng Tin</button>
          </form>
        </div>
        <!-- <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div> -->
      </div>
    </div>
</div>	 
</div>	
@endif
@endsection
@section('script')
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC7I839c4Padwja-_7ZIvtY_RXAnGt3xnM&callback=initMap">
    </script>
    <script type="text/javascript">
    $(document).ready(function() {
      $('#pin').val(0);
      $('#status').val(0);
      @if(isset($name))
        showDistrict($('.city_select').val(),'{{$name->district_id}}');
        // $('.districtsselect option[value={{$name->district_id}}]').attr('selected','selected');

      @endif
    });
    $('#pin').click(function() {
      if ($('#pin').is(":checked"))
      {
        $('#pin').val(1);
      }else
      $('#pin').val(0);
    });
    $('#status').click(function() {
      if ($('#status').is(":checked"))
      {
        $('#status').val(1);
      }else
      $('#status').val(0);
    });
    // district
     $('.city_select').change(function(){
          showDistrict(this.value)
      });
        // CKEDITOR.replace( 'content' );
     function showDistrict(valueCity,selected=null) {
        $.ajax({
        url: "{{route('user.city.searchDistrict')}}",
        type: 'POST',
        data:{id:valueCity, _token: '{{csrf_token()}}'},
        success:function(data){
          var json = $.parseJSON(data);
          $('.districtsselect').html('');
          allContent = '<option>Chọn Quận</option>';
          $.each(json, function(index, val) {
            if(selected != null && selected == val.id)
            {
            content = '<option value="'+val.id+'" selected>'+val.name+'</option>';
            }
            else
            {
            content = '<option value="'+val.id+'">'+val.name+'</option>';
            }
            allContent = allContent + content;
          });
          $('.districtsselect').html(allContent);
        }
      })
     }

     function initMap() {
  // The location of Uluru
  var uluru = {lat: 10.737960, lng: 106.677909};
  // The map, centered at Uluru
  var map = new google.maps.Map(
      document.getElementById('map'), {zoom: 14, center: uluru});
  //console.log(map);
  // The marker, positioned at Uluru
  var marker = new google.maps.Marker({position: uluru, map: map});
  // console.log(marker);
//   google.maps.event.addListener(map, 'click', function(event) {
//    placeMarker(event.latLng);
// });
//   google.maps.event.addListener(map, 'click', function( event ){
 

// });
//   function placeMarker(location) {
//     var marker = new google.maps.Marker({
//         position: location, 
//         map: map
//     });
// }
var marker;
google.maps.event.addListener(map, 'click', function(event) {
  // if(infowindow == null){
  //   var infowindow = new google.maps.InfoWindow({
  //       content: String(event.latLng)
  
  //   });
  // }
     alert( "Latitude: "+event.latLng.lat()+" "+", longitude: "+event.latLng.lng() ); 
   placeMarker(event.latLng);
   console.log(event.latLng.lat());
   console.log(event.latLng.lng());
   $('#lat').val(event.latLng.lat());
   $('#long').val(event.latLng.lng());
    // infowindow.open(map, marker);

});



function placeMarker(location) {

 if (marker == null)
 {
   marker = new google.maps.Marker({
      position: location,
      map: map
  }); } else {   marker.setPosition(location); } }

}


  $('.btn_addimg').click(function(){
  var countImage = $('.Add_img').find('.item-image').length + 1 ;
        if(countImage > 6) {
          alert("Không được quá 6 tấm hình");
        }else{
            $('.Add_img').append(
            '<div class="row item-image" style="border:2px solid;margin-bottom:10px;">'  
          +'<div class="col col-md-4">'
          + '<label for="file-input" class=" form-control-label">File input</label> '
          + '<input type="file" id="file-input'+countImage + '" name="image_upload filesTest" class="form-control-file upload-image" onchange="UploadImage(this);">'
          +'</div>'
          +'<div class="col col-md-6">'
          +'<label for="file-input" class=" form-control-label">Image view</label><br>'
          +'<span class="imageResult">'
          +'<img src="/uploads/upload.png" class="image_show" id="image_show'+countImage+'" width="100px" height="100px">'
          +'<input type="hidden" name="image[]" id="images_show_data'+countImage+'" value="">'
          +'</span>'
          +'</div>'
          +'<div class="col col-md-2" style="text-align: center;padding: 40px;">'
          +'<button type="button" class="btn btn-danger" onclick="deleteItemimage(this);">Xóa</button>'  
          +'</div>'
          +'</div>'
          )
        }
      });

  function deleteItemimage(e){
   var d =  $(e).parent();
   $(d).parent('.item-image').remove();
   var num = +$(".btn_addimg").val() - 1;
    $(".btn_addimg").val(num);
  }

  function UploadImage(e){
  
   var a = $(e).parent('div').parent('div').find('span.imageResult');
   var b = $(a).children('img').attr('id');
   var c = $(a).children('input').attr('id');
   var b2 = '#'+b;
   var c2 = '#'+c;
    var form_data = new FormData();
        form_data.append('file', e.files[0]);
        form_data.append('_token', '{{csrf_token()}}');
        $.ajax({
        url: "{{route('user.uploadimage',['type'=>'user'])}}", // Url to which the request is send
        data: form_data,
        type: 'POST',
        contentType: false,
        processData: false,
        success: function(res)   // A function to be called if request succeeds
        {
          console.log(res);
          $(b2).attr('src','');
          $(c2).val('');
          dataSuccess = JSON.parse(res);
          console.log(res);
          console.log(dataSuccess.filename);
          $(b2).attr('src', '/'+dataSuccess.filename);
          $(c2).val(dataSuccess.filename);
        },error: function(error){
            alert(error);
            }
        });
        };  
    </script>
@endsection