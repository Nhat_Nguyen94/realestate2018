 @extends('realestate.layouts.master')
@section('style')
<script src="/realestate/lib/tinymce/tinymce.min.js"></script>
@endsection
@section('content')
@if(Auth::check())
<div id="content">
  <div class="container">
    <div class="row justify-content-md-center">
          <div class="col col-lg-12 col-xl-10">
        <div class="row has-sidebar">
		@include('realestate.users.menuleft')		
          <div class="col-md-7 col-lg-8 col-xl-8">
            <div class="page-header bordered">
              <h1>Change Password</h1>
            </div>
            <form action="{{route('user.changepassword')}}" method="post">
            	   @csrf
                    @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
              @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
          @endif
                  <input type="hidden" name="_method" value="PUT">  
              <div class="form-group">
                <label>Mật khẩu cũ</label>
                <input type="password" name="password_current" class="form-control form-control-lg" placeholder="Mật khẩu cũ" autofocus>
              </div>
              <div class="form-group">
                <label>Mật khẩu mới</label>
                <input type="password" name="password_new" class="form-control form-control-lg" placeholder="Mật khẩu mới">
              </div>
              <div class="form-group">
                <label>Xác nhận mật khẩu mới</label>
                <input type="password" name="password_new_confirmation" class="form-control form-control-lg" placeholder="Xác nhận mật khẩu mới">
              </div>
              <hr>
              <div class="form-group action">
                <button type="submit" class="btn btn-lg btn-primary">Cập Nhật mật khẩu</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endif
@endsection