 @extends('realestate.layouts.master')
@section('content')
<div id="content">
<div class="row">
@include('realestate.users.menuleft')
<div class="col-md-7">
	 <div class="container-fluid">
      <!-- Breadcrumbs-->
      <!-- <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Tin Đăng</a>
        </li> -->
     <!--  
      </ol> -->
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Danh sách các liên lạc của tin đã đăng</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
					<th>Tên tin tức</th>
					<th>Nội dung liên lạc</th>
					<th>Tên Người gửi</th>
					<th>Số điện thoại người gửi</th>
					<th>Email người gửi</th>
                </tr>
              </thead>
				@foreach($contact as $value)
					<tr>
						<td>{{$value->article->title}}</td>
						<td>{{$value->content}}</td>
						<td>{{$value->user->name}}</td>
						<td>{{$value->user->phone}}</td>
						<td>{{$value->user->email}}</td>

					</tr>
				@endforeach
            </table>
          </div>
        </div>
       <!--  <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div> -->
      </div>
    </div>
</div>	 
</div>	
@endsection
