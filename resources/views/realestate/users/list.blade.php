  @extends('realestate.layouts.master')

@section('content')
@if(Auth::check())
<div id="content">
<div class="row">
@include('realestate.users.menuleft')
<div class="col-md-7">
	 <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Tin Đăng</a>
        </li>
      
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Bảng Tin Đăng</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>STT</th>
                  <th>ID Tin Đăng </th>
                  <th>Tiêu Đề</th>
                  <th>Tóm Tắt</th>
                  <th>Thể Loại</th>
                  <th>User</th>
                  <th>Quận Huyện</th>
                  <th>Trạng Thái</th>
                </tr>
              </thead>
                 @foreach($article as $key => $value)
                <tr>
                
                <td>{{$key+1}}</td>
                <td>{{$value->id}}</td>
                <td>{{$value->title}}</td>
                <td>{{$value->recommend}}</td>
                <td>{{$value->category->name}}</td>
                <td>{{$value->user_id}} </td>
                <td>{{$value->district->name}}</td>
                <td>@if($value->status == '1' )
                    Hiện
                    @else
                    Ẩn
                    @endif
                </td>
                </tr>
                 @endforeach
            </table>
          </div>
          <div class="row justify-content-md-center" >          
              {!! $article->links() !!}
        </div>
        </div>
        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
      </div>
    </div>
</div>	 
</div>	
@endif
@endsection
@section('script')
	 <!--  <script src="/admin-css/vendor/jquery/jquery.min.js"></script>
    <script src="/admin-css/vendor/bootstrap/js/bootstrap.bundle.min.js"></script> -->
    <!-- Core plugin JavaScript-->
   <!--  <script src="/admin-css/vendor/jquery-easing/jquery.easing.min.js"></script> -->
    <!-- Page level plugin JavaScript-->
  <!--   <script src="/admin-css/vendor/datatables/jquery.dataTables.js"></script> -->
  <!--   <script src="/admin-css/vendor/datatables/dataTables.bootstrap4.js"></script> -->
    <!-- Custom scripts for all pages-->
<!--     <script src="/admin-css/js/sb-admin.min.js"></script> -->
    <!-- Custom scripts for this page-->
  <!--   <script src="/admin-css/js/sb-admin-datatables.min.js"></script> -->
@endsection
