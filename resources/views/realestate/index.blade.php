@extends('realestate.layouts.master')

@section('content')
<style>
  .pagination{
    margin :0 auto !important;
  }
 /* #img_col_3 {
    width: 100%;
    position: fixed;
  }*/

 /*   #banner_qc {
    width: 150px;
    height: 500px;
    position: fixed;
    animation-name: mymove;
    animation-duration: 4s;
    animation-iteration-count: infinite;
}

    @keyframes mymove {
    0%   {background-image: url('../realestate/img/left.png'); left:0px; top:0px;}
    25%  {background-image: url('timthumb.png'); left:200px; top:0px;}
    50%  {background-image: url('download.jpg'); left:200px; top:200px;}
    75%  {background-image: url('timthumb.png'); left:0px; top:200px;}
    100% {background-image: url('../realestate/img/left.png'); left:0px; top:0px;}*/
}
</style>
 <div id="content">
    <div class="container">
    <div class="row justify-content-md-center">
          <div class="col col-lg-12 col-xl-10">
        <div class="row">
          <div class="col-md-4 col-lg-3" style="position: relative;">
           <!--  <div id="banner_qc"></div> -->
         <!--    <img src="http://1.bp.blogspot.com/-Jilwdeb5mGA/VdU58vFr2WI/AAAAAAAAAvw/pOIQhehavwI/s1600/hinh-nen-hoat-hinh-Sword-Art-Online-1-3.png" alt="" id="img_col_3"> -->
          </div>
          <div class="col-md-8 col-lg-9">
            <div class="clearfix"></div>
            <div class="item-listing list">
              @foreach($article as $val)
              <div class="item">
                <div class="row">
                  <div class="col-lg-5">
                    <div class="item-image"> <a href="{{route('realestate.detail',['id'=>$val->id])}}"><img src="/{{$val->images[0]->src}}" class="img-fluid" alt="" style="height: 200px;">
                      <!-- <div class="item-badges">
                      <div class="item-badge-left">Sponsored</div>
                      <div class="item-badge-right">For Sale</div>
                      </div> -->
                      <div class="item-meta">
                      <div class="item-price">${{ number_format($val->price) }} VNĐ
                      <small></small>
                      </div>
                      </div>
                      </a>
                      @if($val->pin == 1)
                      <a href="javascript:void(0)" class="save-item"
                      data-toggle="popover" data-placement="top" data-content="Tin ưu tiên"
                      ><i class="fa fa-star"></i></a>
                      @endif 
                      </div>
                  </div>
                  <div class="col-lg-7">
                  <div class="item-info">
                    <h3 class="item-title"><a href="property_single.html">{{$val->title}}</a></h3>
                    <div class="row">
                      <div class="col-md-6">
                           <div class="item-location"><i class="fa fa-map-marker"></i> Địa Chỉ: {{$val->address}}</div>
                      </div>
                      <div class="col-md-6">
                           <div class="item-location"><i class="fa fa-check-circle"></i> Giấy Tờ: {{$val->document}}</div>
                      </div>
                    </div>
                     <div class="row">
                      <div class="col-md-6">
                         <div class="item-location"><i class="fa fa-home"></i> Diện Tích: {{$val->area}} m2</div>
                      </div>
                      <div class="col-md-6">
                           <div class="item-location"><i class="fa fa-arrows"></i> Hướng: {{$val->direction}}</div>
                      </div>
                    </div>
                    <div class="item-details-i">Số Phòng Ngủ: <span class="bedrooms" data-toggle="tooltip" title="{{$val->bedroom}} Phòng Ngù">{{$val->bedroom}} <i class="fa fa-bed"></i></span> Số Phòng Tắm:<span class="bathrooms" data-toggle="tooltip" title="{{$val->bathroom}}  Phòng Tắm"> {{$val->bathroom}} <i class="fa fa-bath"></i></span>

                     </div>
                    <div class="item-details">
                      <ul>
                        <!-- <li>Sq Ft <span>730-2600</span></li> -->
                        <li>Loại Bất Động Sản <span>{{$val->category->name}}</span></li>
                        <li>Quận Huyện<span>{{$val->district->name}}</span></li>
                      </ul>
                    </div>
                 </div>
                    <div class="row">
                    <div class="col-md-6">
                                        <div class="added-on">Đăng ngày {{date('d-m-Y', strtotime($val->created_at))}} </div>

                    </div>
                    <div class="col-md-6">
                                         <a href="#" class="added-by">bởi {{$val->user->name}}</a>

                    </div>
                    </div>
                  </div>
                </div>
              </div>
            @endforeach

            </div>

          </div>

          <!-- <div class="row justify-content-md-center" style="width: 100%;">
               <nav aria-label="Page navigation">
              <ul class="pagination">
                <li class="page-item"><a class="page-link" href="#">&laquo;</a></li>
                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item"><a class="page-link" href="#">&raquo;</a></li>
              </ul>
            </nav>
          </div> -->

        </div>
        </div>
        </div>
        </div>
        </div>
        <div class="col-md-12">
               <div class="row">
              {!! $article->links('paginate') !!}
              </div>
        </div>
<script src="/realestate/lib/sidr/jquery.sidr.min.js"></script>
<script>
  $(document).ready(function() {
  $('#toggle-filters').sidr({
    side: 'left',
    displace : false,
    renaming : false,
    name: 'sidebar',
    source: function() {
      AOS.refresh();
    },

  });
  $(function () {
  $('[data-toggle="popover"]').popover()
})
});
  </script>
   
@endsection
